# Niv'Eaux, partage d'informations sur les niveaux d'eau 

Créé en 2016 par l'EPTB de la Sèvre Nantaise, cet outil web collaboratif centralise les données utiles pour le suivi des niveaux d’eau en cas de crue. 
La collecte des données disponibles sur internet (vigicrues, pluviomètres...) est réalisée automatiquement par le biais de tâches planifiées. 
Ces données peuvent être complétées par des relevés locaux effectués par les utilisateurs de Niv'Eaux et saisis directement dans l'outil.
Les utilisateurs peuvent paramétrer des notifications à déclencher sur la station et la hauteur souhaitées. 
En cas de dépassement de hauteur, une notification leur est automatiquement transmise par SMS ou mail.

![Niv'Eaux](https://photos.sevre-nantaise.com/i.php?galleries/dossiers_photos/srenou/2018_05_24_outil_suivi_niveau/niveaux_montage-me.jpg)

## Changelog

Mises à jour, version 1.1 (April 16, 2020)

* Corrections de bugs à l'installation et à l'execution
* Intégration de données issues de l'API hydrométrie
* Type de données relevables "débits"
* Fonction intermédiaire à executer lors du déclenchement des notifications (valeur brute ou moyenne sur durée)
* Nouveaux scripts de création et update de la base de données
* Compléments sur le readme

## Démo

Une démo de l'outil est accessible à l'adresse : http://niveaux-demo.sevre-nantaise.com/

## Installation

NB : Pour une mise à jour, voir la procédure en fin de Readme.

### Pré-requis

* Php: >=5.6
* PostGRE: 9.6
* PostGIS: 2.4

L'outil utilise l'API OVH pour l'envoi de SMS, à modifier si vous utilisez un autre service.

Lors de l'installation il faudra paramétrer une tâche CRON sur le serveur pour automatiser les relevés.

#### Récupération du code et dépôt sur le serveur

Téléchargez la dernière version du code : http://gitlab.sevre-nantaise.com/eptbsn/niveaux-foss/repository/master/archive.zip 

Dézippez les fichiers sur votre serveur web. 

Utilisez *composer* pour charger les dépendances :
* Téléchargez *composer* : https://getcomposer.org/
* Déposez composer.phar à la racine de votre projet, au même emplacement que *composer.json*
* Exécutez 

```sh
$ php composer.phar install
```

* *composer* va télécharger les dépendances dans /vendor

#### Création de la base de données

Créez une base de données (ou utilisez en une existante, dans ce cas assurez vous qu'un schéma *niveaux* n'existe pas déjà).

Récupérez le code SQL de création des tables Niv'Eaux dans le fichiers /tmp/instal_sql.php

Depuis PGAdmin, avec un utilisateur ayant les droits suffisants, exécutez le code sql.

Ce code va :
* Créer un schéma *niveaux*
* Créer les tables et les peupler avec des données test
* Créer un user Niv'Eaux admin

SUPRRIMEZ le dossier /tmp

**Bonne pratique :**
Créez un user PostGRE spécifique pour l'outil Niv'Eaux, et accordez lui les droits d'usage sur le schéma niveaux, et les droits complets sur les tables et séquences. Utilisez ce user pour paramétrer les informations de connexion à la base de données dans le fichier de configuration de l'étape suivante.

#### Fichier de configuration

A partir du fichier /utils/conf_template.php, créer un fichier /utils/conf.php contenant les informations utiles pour faire tourner l'application, notamment :
* Informations de connexion à la base de données
* Identifiants du service de mail à utiliser pour l'envoi des notifications
* Identifiants de l'API OVH pour l'envoi des SMS (code à modifier si vous utiliez un autre service)


#### Configuration Apache

Assurez-vous que votre nom de domaine pointe bien vers votre dossier d'installation de Niv'Eaux **/public**.

## Mise à jour

Depuis une installation en v1.0, suivre les étapes suivantes :
* Sauvegardez l'ensemble du code source (optionnel si vous utilisez un nouveau dossier d'installation)
* Sauvegardez l'ensemble de la base de données (obligatoire)
* Suivez la procédure d'installation de l'outil en utilisant un nouveau dossier d'installation
* Appliquez les modifications sur la base de données :
    * Récupérez le code SQL de mise à jour dans le fichiers /tmp/update_sql.php
    * Depuis PGAdmin, avec un utilisateur ayant les droits suffisants, exécutez le code sql.
* SUPPRIMEZ le dossier /tmp
* Assurez-vous que votre nom de domaine pointe bien vers votre nouveau dossier d'installation

## Première connexion

Une fois l'installation terminée, vous pouvez vous connecter à l'application avec le compte admin / passe admin.

**Pensez à modifier le mot de passe du compte admin dès la première connexion !**

Modifiez les paramétrages de l'application depuis la page *Administration/Paramètres*

Déclarez vos points de suivi *Points de suivi/Nouveau point de suivi*

## Relevés automatiques

Dans la base de données, dans la table *tache_planifiee*, sur la base des modèles déjà présents dans la table, créez vos tâches de type relevés vigicrue, API hydrométrie ou infoclimat.
Vous pouvez rendre inactives les tâches modèles (actif = FALSE).
NE SUPPRIMEZ pas la tâche *controle notification*.

Sur votre serveur web, créez une tâche CRON, planifiée au rythme souhaité (par exemple toutes les 10 minutes), executant la commande suivante :
```sh
$ php /XXXXXXX/Niveaux/Console/console.php cron
```
Une fois la tâche CRON créée, les relevés vont commencer à s'effectuer automatiquement.
Une fois que vous vous êtes assurés que les relevés s'effectuent correctement, vous pourrez activer la tâche *controle notification* (actif = TRUE dans la BDD) qui déclenche l'envoi des notifications aux utilisateurs.


## Contributions

Créez un compte sur notre plateforme Gitlab pour ouvrir des tickets (signaler les bugs) ou proposer des améliorations du code : http://gitlab.sevre-nantaise.com/users/sign_in

## Auteur

Sébastien RENOU - EPTB Sèvre Nantaise
Si vous utilisez l'outil ou si vous avez des questions, n'hésitez pas à me contacter : https://www.sevre-nantaise.com/contact?contact=srenou

## Licence

Ce projet est sous licence AGPL V3

