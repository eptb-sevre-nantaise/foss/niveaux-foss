<?php

/*
 * Fichier de configuration à compléter et à renommer en /utils/conf.php
 */

$debug = false;

if ($debug) {

    ini_set('display_errors', 'on');
    error_reporting(E_ALL);
}

define('DEBUG', $debug);


define('PATH', __DIR__.'/../') ;

// Informations de connexion à la base de données PostgreSQL
define('DBHOST', 'xxx');
define('DBNAME', 'xxx');
define('SCHEMA', 'xxx');
define('DBPORT', 5432);
define('DBUSER', 'xxx');
define('DBPWD', 'xxx');

// remember key, clé aléatoire pour le cookie
define('REMEMBERKEY', 'xxx');

// MAIL
define('MAILADMIN', 'admin@test.com'); 
define('MAILEXP', 'expediteur@test.com');
define('NAMEEXP', 'Expediteur');
define('MAILHOST', 'smtp.xxx.xxx');
define('MAILPORT', 587);
define('MAILSECURE', 'tls');
define('MAILUSERNAME', 'comptemail@test.com');
define('MAILPASS', 'passe_compte_mail');

// SMS API OVH
/*
 * si utilisation d'une autre API que celle d'OVH pour l'envoi des SMS, il faudra peut être revoir le code
 */
define('SMSAPIKEY', 'xxx');
define('SMSAPISECRET', 'xxx');
define('SMSAPICONSKEY', 'xxx');
define('SMSENDPOINT', 'ovh-eu');
define('SENDER', 'sender name as declared in your SMS service provider');

// METEOFRANCE
// API APPLICATION_ID

define('MF_APPLICATION_ID', '********************') ;
