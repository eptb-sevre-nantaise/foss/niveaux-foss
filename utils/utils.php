<?php

/**
 * envoi d'un SMS avec l'API d'OVH
 * OLD WAY
 * @param string $username
 * @param string $tel_dest  
 * @param string $message
 * @param silexApp $app
 */

 /*
function envoiSMS($username, $tel_dest, $message, $app) {

    // vérifie 

    $Util_dest = new Utilisateur($app['pdo'], $username);


    if ($Util_dest->nombreEnvoisNotifSMS($app['SMSDELAY'], $app) <= $app['SMSMAX']) {

        if ($Util_dest->nombreEnvoisNotifSMS($app['SMSDELAY'], $app) == ($app['SMSMAX'] - 1)) {
            $message .= ' LIMITE ATTEINTE. Vous ne recevez plus d\'autre SMS avant ' . str_replace('hours', ' heures', $app['SMSDELAY']) . '.';
        }

        // Init SmsApi object
        $Sms = new \Ovh\Sms\SmsApi(SMSAPIKEY, SMSAPISECRET, SMSENDPOINT, SMSAPICONSKEY);

        // Get available SMS accounts
        $accounts = $Sms->getAccounts();

        // Set the account you will use
        $Sms->setAccount($accounts[0]);

        // Get declared senders
        $senders = $Sms->getSenders();

        // Create a new message
        $Message = $Sms->createMessage();
        $Message->setSender($senders[0]);
        $Message->addReceiver($tel_dest);
        $Message->setIsMarketing(false);

        if ($Message->send($message)) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

*/

// SMS OVH
use \Ovh\Api;

/**
 * update de la fonction d'envoi d'un SMS avec l'API d'OVH
 * @param string $username
 * @param string $tel_dest  
 * @param string $message
 * @param silexApp $app
 */
function envoiSMS2($username, $tel_dest, $message, $app) {

    // vérifie 

    $Util_dest = new Utilisateur($app['pdo'], $username);


    if ($Util_dest->nombreEnvoisNotifSMS($app['SMSDELAY'], $app) <= $app['SMSMAX']) {

        if ($Util_dest->nombreEnvoisNotifSMS($app['SMSDELAY'], $app) == ($app['SMSMAX'] - 1)) {
            $message .= ' LIMITE ATTEINTE. Vous ne recevez plus d\'autre SMS avant ' . str_replace('hours', ' heures', $app['SMSDELAY']) . '.';
        }

        // NEW ///

        $endpoint = SMSENDPOINT;
        $applicationKey = SMSAPIKEY;
        $applicationSecret = SMSAPISECRET;
        $consumer_key = SMSAPICONSKEY;
       
    
        $conn = new Api(
            $applicationKey,
            $applicationSecret,
            $endpoint,
            $consumer_key
        );

    
        $smsServices = $conn->get('/sms/');
        /*
        foreach ($smsServices as $smsService) {    
            print_r($smsService);
        }
    */
        $content = (object) array(
            "charset" => "UTF-8",
            "class" => "phoneDisplay",
            "coding" => "7bit",
            "message" => $message,
            "noStopClause" => true,
            "priority" => "high",
            "receivers" => [$tel_dest],
            "sender" => SENDER,
            "senderForResponse" => false,
            "validityPeriod" => 2880
        );
        $resultPostJob = $conn->post('/sms/' . $smsServices[0] . '/jobs', $content);
    
        if (count($resultPostJob['validReceivers'])==1) {
            return true ;
        } else {
            return false ;
        }
        
    
        //$smsJobs = $conn->get('/sms/' . $smsServices[0] . '/jobs');
        //print_r($smsJobs);



    } else {
        return false;
    }
}





function testSMS() {   
   
        $endpoint = SMSENDPOINT;
        $applicationKey = SMSAPIKEY;
        $applicationSecret = SMSAPISECRET;
        $consumer_key = SMSAPICONSKEY;

        $conn = new Api(    $applicationKey,
                            $applicationSecret,
                            $endpoint,
                            $consumer_key);

        $smsServices = $conn->get('/sms/');
        foreach ($smsServices as $smsService) {

            print_r($smsService);
        }
}

function currentUser($app) {
    $token = $app['security.token_storage']->getToken();
    if (null !== $token) {
        return $token->getUser();
    } else {
        return false;
    }
}

function chargeLayers($layers) {

    $tab = array();
    $tabL = explode(';', $layers);

    foreach ($tabL as $key => $L) {
        $l = explode('/', $L);
        $tab[$l[0]] = $l[1];
    }
    return $tab;
}

/*
 * récupère la liste des paramètres de l'application, leurs types et valeurs
 */

function chargeParametresAppli($pdo) {

    $qry = $pdo->prepare('select * from ' . SCHEMA . '.parametres order by ordre asc');
    $qry->execute();
    return $qry->fetchAll(PDO::FETCH_ASSOC);
}

/*
 * @Object P Point de suivi
 * @Date debut
 * @Date fin
 */

function calculeCumul($P, $debut, $fin, $pdo) {

    $qry = $pdo->prepare('select sum(valeur) as cumul from ' . SCHEMA . '.releve_valeur where id_point_suivi = :idp and date_valeur >= :deb and date_valeur <= :fin');
    $qry->bindParam(':idp', $P->id_point_suivi, PDO::PARAM_INT);
    $qry->bindParam(':deb', $debut);
    $qry->bindParam(':fin', $fin);
    $qry->execute();
    $res = $qry->fetchObject();
    return $res->cumul;
}

function chargeEnvoisNotifications($app, $depuis = '-1 week') {

    $pdo = $app['pdo'];
    $now = new DateTime('now', new DateTimeZone($app['TIMEZONE']));

    $qry = $pdo->prepare('select * from ' . SCHEMA . '.notification_envoi_infos where date_envoi_notification > \'' . $now->modify($depuis)->format('Y-m-d H:i:s') . '\' order  by date_envoi_notification desc');

    $qry->execute();
    return $qry->fetchAll(PDO::FETCH_ASSOC);
}

function chargeNotifications($pdo) {

    $qry = $pdo->prepare('select *  from ' . SCHEMA . '.notification_infos order by username asc');

    $qry->execute();
    return $qry->fetchAll(PDO::FETCH_ASSOC);
}

/*
 * 
 * @param pdo 
 * @param string type_suivi
 * @param string type_releve
 * @return array()
 * 
 */

function chargeReleves($pdo, $type_suivi = null, $type_releve = null, $limit = 50) {

    $cond = null;

    if ($type_suivi != null) {
        $cond = ' AND type_suivi = :type_suivi';
    }
    if ($type_releve != null) {
        $cond = ' AND type_releve = :type_releve';
    }

    $qry = $pdo->prepare('select * from ' . SCHEMA . '.releve r '
            . 'left join ' . SCHEMA . '.tache_planifiee t on t.id_tache = r.id_tache '
            . 'left join ' . SCHEMA . '.utilisateur u on r.id_user = u.id_user '
            . 'where 1 = 1 ' . $cond . ' '
            . 'order by date_releve desc LIMIT ' . $limit);


    if ($type_suivi != null) {
        $qry->bindParam(':type_suivi', $type_suivi, PDO::PARAM_STR);
    }
    if ($type_releve != null) {
        $qry->bindParam(':type_releve', $type_releve, PDO::PARAM_STR);
    }
    $qry->execute();

    $tabR = array();
    $compt = 0;
    while ($r = $qry->fetchObject()) {

        $tabR[$compt]['Releve'] = $r;
        $tabR[$compt]['Valeurs'] = chargeValeursReleve($r->id_releve, $pdo);
        $compt++;
    }

    return $tabR;
}

function chargeValeursReleve($id_releve, $pdo) {

    $qry = $pdo->prepare('select count(*) as compte, v.id_point_suivi, p.point_suivi, p.alias, p.type_point from ' . SCHEMA . '.releve_valeur v '
            . 'left join ' . SCHEMA . '.point_suivi p on p.id_point_suivi = v.id_point_suivi '
            . 'where v.id_releve = :id_releve '
            . 'group by v.id_point_suivi, p.point_suivi, p.alias, p.type_point ');

    $qry->bindParam(':id_releve', $id_releve, PDO::PARAM_INT);
    $qry->execute();
    return $qry->fetchAll(PDO::FETCH_ASSOC);
}

/*
 * recherche les taches planifiées dont la dernière execution est antérieure à now - fréquence exec ou dont le statut dern exec est echec
 */

function chargeTachesAExecuter($pdo) {

    $qry = $pdo->prepare('select * from ' . SCHEMA . '.tache_planifiee where ((date_derniere_execution + interval \'1 minute\' * frequence_execution) < :now or statut_derniere_execution = \'echec\' or date_derniere_execution is null) and actif is true order by ordre asc');
    $now = new \DateTime('now');
    $date_n = $now->format('Y-m-d H:i:s');
    $qry->bindParam(':now', $date_n);
    $qry->execute();
    return $qry->fetchAll(PDO::FETCH_ASSOC);
}

/*
 * contrôle si la valeur soumise n'est pas déjà dans la bdd
 */

function estUnDoublon($valeur, $id_point_suivi, $date_valeur, $type_releve, $type_suivi, $pdo) {

    $qry = $pdo->prepare('select * from ' . SCHEMA . '.releve_valeur v left join ' . SCHEMA . '.releve r on r.id_releve = v.id_releve '
            . 'where v.valeur = :valeur and v.date_valeur = :date_valeur and v.id_point_suivi = :id_point_suivi and r.type_releve = :type_releve and r.type_suivi = :type_suivi');
    $qry->bindParam(':valeur', $valeur);
    $qry->bindParam(':date_valeur', $date_valeur);
    $qry->bindParam(':id_point_suivi', $id_point_suivi, PDO::PARAM_INT);
    $qry->bindParam('type_releve', $type_releve);
    $qry->bindParam('type_suivi', $type_suivi);
    $qry->execute() or die('Erreur estUnDoublon');
    if ($qry->rowCount() > 0) {
        return true;
    } else {
        return false;
    }
}

/*
 * contrôle si la on a déjà une valeur mais différente pour la date et le point de suivi fournis
 * 
 */

function aUneValeurDifferente($valeur, $id_point_suivi, $date_valeur, $type_releve, $type_suivi, $pdo) {

    $qry = $pdo->prepare('select * from ' . SCHEMA . '.releve_valeur v left join ' . SCHEMA . '.releve r on r.id_releve = v.id_releve '
            . 'where v.valeur != :valeur and v.date_valeur = :date_valeur and v.id_point_suivi = :id_point_suivi and r.type_releve = :type_releve and r.type_suivi = :type_suivi');
    $qry->bindParam(':valeur', $valeur);
    $qry->bindParam(':date_valeur', $date_valeur);
    $qry->bindParam(':id_point_suivi', $id_point_suivi, PDO::PARAM_INT);
    $qry->bindParam('type_releve', $type_releve);
    $qry->bindParam('type_suivi', $type_suivi);
    $qry->execute() or die('Erreur aUneValeurDifferente');
    if ($qry->rowCount() > 0) {
        return true;
    } else {
        return false;
    }
}

/*
 * retourne la valeur actuelle si elle est différente de celle proposée
 */

function valeurActuelleSiDifferente($valeur, $id_point_suivi, $date_valeur, $type_releve, $type_suivi, $pdo) {

    $qry = $pdo->prepare('select * from ' . SCHEMA . '.releve_valeur v left join ' . SCHEMA . '.releve r on r.id_releve = v.id_releve '
            . 'where v.valeur != :valeur and v.date_valeur = :date_valeur and v.id_point_suivi = :id_point_suivi and r.type_releve = :type_releve and r.type_suivi = :type_suivi');
    $qry->bindParam(':valeur', $valeur);
    $qry->bindParam(':date_valeur', $date_valeur);
    $qry->bindParam(':id_point_suivi', $id_point_suivi, PDO::PARAM_INT);
    $qry->bindParam('type_releve', $type_releve);
    $qry->bindParam('type_suivi', $type_suivi);
    $qry->execute();
    if ($qry->rowCount() > 0) {
        $s = $qry->fetchObject();
        return $s;
    } else {
        return false;
    }
}

function estVide($valeur) {

    if ($valeur === null or $valeur === '') {
        return true;
    } else {
        return false;
    }
}

function rechercheStationParNomOfficiel($st, $pdo) {

    $qry = $pdo->prepare('select * from ' . SCHEMA . '.point_suivi where nom_station_officiel = :st');
    $qry->bindParam(':st', $st, PDO::PARAM_STR);
    $qry->execute();
    return $qry->fetch();
}

function rechercheStationParCodeStation($st, $pdo) {

    $qry = $pdo->prepare('select * from ' . SCHEMA . '.point_suivi where code_station = :st');
    $qry->bindParam(':st', $st, PDO::PARAM_STR);
    $qry->execute();
    return $qry->fetch();
}

/*
 * function chargeUtilisateurs
 */

function chargeUtilisateurs($pdo) {

    $qry = $pdo->prepare('select * from ' . SCHEMA . '.utilisateur order by nom asc');
    $qry->execute();
    return $qry->fetchAll(PDO::FETCH_ASSOC);
}

function chargePointsSuivi($id_user, $pdo) {

    $qry = $pdo->prepare('select p.id_point_suivi,p.alias,p.type_point,p.point_suivi,p.unite,l.id_point_suivi as idp, ST_asText(ST_Transform(geom,4326)) as geom_ps from ' . SCHEMA . '.point_suivi_infos p '
            . 'left join ' . SCHEMA . '.l_user_point_suivi_favori l on l.id_point_suivi = p.id_point_suivi and l.id_user = :id_user '
            . 'order by idp asc, point_suivi asc');
    $qry->bindParam(':id_user', $id_user, PDO::PARAM_INT);
    $qry->execute();
    $tabP = $tabV = array();
    while ($p = $qry->fetch(PDO::FETCH_ASSOC)) {

        $tabV = derniereValeur($p['id_point_suivi'], new DateTime('now'), $pdo, 2);
        $p['avant_dern_valeur'] = $tabV[1];
        $p['dern_valeur'] = $tabV[0];
        $tabP[] = $p;
    }

    return $tabP;
}

function chargeListePointsSuivi($pdo, Utilisateur $U = null) {

    $qry = $pdo->prepare('select *, ST_asText(ST_Transform(geom,4326)) as geom_ps from ' . SCHEMA . '.point_suivi_infos order by point_suivi asc');
    $qry->execute();
    $tabP = array();
    while ($p = $qry->fetchObject()) {
        if ($U != null and $U->aAcces('ROLE_LECTEUR', $p->id_point_suivi, $pdo))
            $tabP[$p->id_point_suivi] = $p->point_suivi;
    }
    return $tabP;
}

function verifieNotifications($id_releve, $id_pt_suivi, $valeur, $date_valeur, $pdo) {

    $Dern = derniereValeur($id_pt_suivi, $date_valeur, $pdo);
    $tabNotif = rechercheNotifications($id_pt_suivi, $valeur, $Dern['valeur'], $pdo);
    if (count($tabNotif) > 0) {
        foreach ($tabNotif as $key => $n) {

            $N = new Notification($pdo, $n['id_notification']);
            $N->envoie($id_releve, $valeur, $date_valeur, $Dern, $pdo);
        }
    }
}

/*
 * function rechercheNotifications
 * 
 * @param integer $id_point_suivi
 * @param string $valeur
 * @param string $dernvaleur
 * @param pdo
 * 
 * @return array() notifications
 */

function rechercheNotifications($id_pt_suivi, $valeur, $dernvaleur, $pdo) {

    echo $id_pt_suivi, $valeur, $dernvaleur;

    // on recherche un notif montante
    if ($valeur - $dernvaleur > 0) {

        $qry = $pdo->prepare('select id_notification from ' . SCHEMA . '.notification where id_point_suivi = :id_point_suivi and montant_descendant = \'montant\' and seuil <= :valeur and seuil > :dernvaleur');
        $qry->bindParam(':id_point_suivi', $id_pt_suivi, PDO::PARAM_INT);
        $qry->bindParam(':valeur', $valeur, PDO::PARAM_STR);
        $qry->bindParam(':dernvaleur', $dernvaleur, PDO::PARAM_STR);
        $qry->execute();
        return $qry->fetchAll(PDO::FETCH_ASSOC);
    } elseif ($valeur - $dernvaleur < 0) {

        $qry = $pdo->prepare('select id_notification from ' . SCHEMA . '.notification where id_point_suivi = :id_point_suivi and montant_descendant = \'descendant\' and seuil >= :valeur and seuil < :dernvaleur');
        $qry->bindParam(':id_point_suivi', $id_pt_suivi, PDO::PARAM_INT);
        $qry->bindParam(':valeur', $valeur, PDO::PARAM_STR);
        $qry->bindParam(':dernvaleur', $dernvaleur, PDO::PARAM_STR);
        $qry->execute();
        return $qry->fetchAll(PDO::FETCH_ASSOC);
    } else {
        return null;
    }
}

function derniereValeur($id_pt_suivi, $date_valeur, $pdo, $limit = 1) {

    $qry = $pdo->prepare('select * from ' . SCHEMA . '.releve_valeur where id_point_suivi = :id_point_suivi and date_valeur < :date_valeur order by date_valeur desc limit ' . $limit);
    $qry->bindParam(':id_point_suivi', $id_pt_suivi, PDO::PARAM_INT);
    $dt = $date_valeur->format('Y-m-d H:i:s');
    $qry->bindParam(':date_valeur', $dt, PDO::PARAM_STR);
    $qry->execute();
    if ($qry->rowCount() === 1) {
        return $qry->fetch();
    } elseif ($qry->rowCount() > 1) {
        return $qry->fetchAll();
    } else {
        return false;
    }
}

// conversion de la géométrie

function convertGeomFromText($pdo, $geom_text) {

    $qry = $pdo->prepare('select ST_GeomFromText(:geom_text,2154) as geom');
    $qry->bindParam(':geom_text', $geom_text, PDO::PARAM_STR);
    $qry->execute();
    $res = $qry->fetch();
    return $res['geom'];
}

/*
 * FONCTIONS UTILES BDD
 */

/*
 * update une colonne d'une table
 */

function updatecol($table, $idcol, $id, $col, $value, $pdo) {


    $qry = $pdo->prepare("update " . SCHEMA . "." . $table . " set " . $col . " = :val where " . $idcol . " = :id");
    $qry->bindParam(':val', $value);
    $qry->bindParam(':id', $id);
    return $qry->execute();
}

/*
 * function prepInsertQuery
 * prépare une liste de paramètre pour une requête PDO d'insert
 * et un tableau des données 
 * 
 * @param Silex request $request
 * @param array exclude tableau des param à exclure de la requête
 * 
 * @return array(liste des params à updater mis en forme , tableau param > valeur) 
 */

function prepInsertQuery($request, $exclude = null) {

    $reqFields = $reqParams = null;
    foreach ($request as $key => $val) {

        if (!isset($exclude[$key])) {
            $reqFields .= $key . ', ';
            $reqParams .= ':' . $key . ', ';
            $tabD[':' . $key] = $val;
        }
    }

    return array('reqFields' => substr($reqFields, 0, -2), 'reqParams' => substr($reqParams, 0, -2), 'tabD' => $tabD);
}

/*
 * function prepUpdateQuery
 * prépare une liste de paramètre pour une requête PDO d'update
 * et un tableau des données 
 * 
 * @param Silex request $request
 * @param string index index de la table à ne pas ajouter dans la liste des params à updater
 * @return array(liste des params à updater mis en forme , tableau param > valeur) 
 */

function prepUpdateQuery($request, $index) {

    $req = null;
    foreach ($request as $key => $val) {
        if ($key != $index)
            $req .= $key . ' = :' . $key . ', ';
        $tabD[':' . $key] = $val;
    }

    return array('req' => substr($req, 0, -2), 'tabD' => $tabD);
}

/*
 * function returnResultQuery(
 * reçoit une requête PDO et se charge de l'executer puis de renvoyer un message selon le résultat
 * @param PDOStatement qry
 * 
 * $param PDO pdo
 * $param tabD données
 * $id integer last ID
 * @return json result
 * 
 */

function returnResultQuery($qry, $pdo, $tabD) {



    try {
        $qry->execute() or die('Erreur SQL ' . implode(',', $pdo->errorInfo()));
    } catch (\Exception $e) {

        $result['status'] = 'error';
        $result['message'] = $pdo->errorInfo();
        $result['data'] = json_encode($tabD) . ' ' . $qry->debugDumpParams();
        //die('erreur');
        return json_encode($result);
    }
    //$qry->debugDumpParams();

    $result['status'] = 'success';
    $res = $qry->fetchObject();
    if (isset($res->id))
        $result['id'] = $res->id;

    //var_dump($result) ;
    return json_encode($result);
}

function envoieMail($dest, $exp, $nomexp, $message, $sujet) {

    $mail = new PHPMailer;
    $mail->SMTPDebug = 0;

    $mail->isSMTP();                                        // Set mailer to use SMTP
    $mail->SMTPAuth = true;                                 // Enable SMTP authentication
    $mail->SMTPSecure = 'tls';                              // Enable encryption, 'ssl' also accepted

    $mail->Host = MAILHOST;                                 // Specify main and backup server
    $mail->Port = MAILPORT;
    $mail->SMTPSecure = MAILSECURE;

    $mail->Username = MAILUSERNAME;                         // SMTP username
    $mail->Password = MAILPASS;                             // SMTP password



    $mail->From = MAILUSERNAME;
    $mail->FromName = $nomexp;

    $mail->addAddress($dest, $dest);                        // Add a recipient

    $mail->addReplyTo($exp, $nomexp);

    $mail->isHTML(true);                                    // Set email format to HTML

    $mail->Subject = utf8_decode($sujet);
    $mail->Body = utf8_decode($message);

    if (!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
        return false;
    }

    echo 'Message has been sent';
    return true;
}

function verifieFonction($app, $params) {

    if ($params['valeur'] == null or empty($params['date_valeur'] or empty($params['fonction']))) {
        die('Paramètres de verifieFonction incomplets / valeur '.$params['valeur'].' / date_valeur '.$params['date_valeur'].' / fonction '.$params['fonction']);
    }

    $fonction = $params['fonction'];
    $res = call_user_func_array($fonction, ['app' => $app, 'params' => $params]);
    $res['message'] = 'Valeur actuelle : ' . $res['valeur_actuelle'] . ' - Dernière valeur : ' . $res['derniere_valeur'];

    if ($params['montant_descendant'] === 'montant'
            and $res['valeur_actuelle'] >= $params['seuil']
            and $res['valeur_actuelle'] > $res['derniere_valeur']
            and $res['derniere_valeur'] < $params['seuil']) {
        $res['result'] = true;
        return $res;
    } elseif ($params['montant_descendant'] === 'descendant'
            and $res['valeur_actuelle'] <= $params['seuil']
            and $res['valeur_actuelle'] < $res['derniere_valeur']
            and $res['derniere_valeur'] > $params['seuil']) {
        $res['result'] = true;
        return $res;
    } else {
        $res['result'] = false;
        return $res;
    }
}

function moyenneSurDuree($app, $params) {

    $moyennePourValeurActuelle = calculeMoyenneSurDuree($app, $params);
    $dernVal = derniereValeur($params['id_point_suivi'], new \DateTime($params['date_valeur']), $app['pdo']);

    $paramsDerniereValeur = $params;
    $paramsDerniereValeur['date_valeur'] = $dernVal['date_valeur'];
    $moyennePourDerniereValeur = calculeMoyenneSurDuree($app, $paramsDerniereValeur);

    return [
        'valeur_actuelle' => $moyennePourValeurActuelle,
        'date_valeur_actuelle' => $params['date_valeur'],
        'derniere_valeur' => $moyennePourDerniereValeur,
        'date_derniere_valeur' => $dernVal['date_valeur']
    ];
}

function calculeMoyenneSurDuree($app, $params) {

    $duree = $params['parametres']['duree'];

    if (empty($duree)) {
        die('Paramètres de moyenneDuree incomplets');
    }
    $date_valeur = new \DateTime($params['date_valeur'], new DateTimeZone($app['TIMEZONE']));
    $date_debut_duree = new \DateTime($params['date_valeur'], new DateTimeZone($app['TIMEZONE']));


    $query = 'select round(avg(v.valeur),3) as moyenne from ' . SCHEMA . '.valeur_infos v '
            . 'where '
            . 'v.date_valeur > \'' . $date_debut_duree->modify($duree)->format('Y-m-d H:i:s') . '\' '
            . 'and v.date_valeur <= \'' . $date_valeur->format('Y-m-d H:i:s') . '\' '
            . 'and v.id_point_suivi = ' . $params['id_point_suivi'];


    $pdo = $app['pdo'];
    $qry = $pdo->prepare($query);
    $qry->execute();
    $res = $qry->fetchObject();
    return $res->moyenne;
}

function valeurActuelleBrute($app, $params) {

    $dernVal = derniereValeur($params['id_point_suivi'], new \DateTime($params['date_valeur']), $app['pdo']);

    return [
        'valeur_actuelle' => $params['valeur'],
        'date_valeur_actuelle' => $params['date_valeur'],
        'derniere_valeur' => $dernVal['valeur'],
        'date_derniere_valeur' => $dernVal['date_valeur']
    ];
}
