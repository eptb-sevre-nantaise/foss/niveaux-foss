<?php

/*
 * Eléments spécifiques de la conf pour la Console
 */


// console provider
use Knp\Provider\ConsoleServiceProvider;
/**
* Console Service Provider
*
*/
$app->register(new ConsoleServiceProvider(), array(
    'console.name' => 'ConsoleApp',
    'console.version' => '1.0.0',
    'console.project_directory' => __DIR__ . '/..'
));


// Commandes NIVEAUX
spl_autoload_register(function ($class)  use ($app) {
    $file =  PATH . 'Niveaux/Commandes/' . str_replace('\\', '/', $class) . '.class.php';    
    if (file_exists($file)) {
        require $file;
    }
});


return $app ;
