<?php

// https://github.com/kendepelchin/silex-base/blob/master/src/Classes/Console/ConsoleCommand.php


use Knp\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportAPIMeteoFranceJson extends Command
{

    protected function configure()
    {
        $this
            ->setName('import-api-meteofrance-json')
            ->setDescription('Collecte des données de l\'API MétéoFrance (obs hoarires) au format json')
            ->setDefinition(
                new InputDefinition(array(
                    new InputArgument('id_tache', InputArgument::REQUIRED),
                    new InputArgument('adresse', InputArgument::REQUIRED),
                ))
            )

        ; // nice, new line
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $app = $this->getSilexApplication();
        $datetime = new DateTime('now', new DateTimeZone($app['TIMEZONE']));
        $T = new TachePlanifiee($app['pdo'], $input->getArgument('id_tache'));

        $date_derniere_execution = new DateTime($T->date_derniere_execution, new DateTimeZone($app['TIMEZONE']));
        $date_derniere_execution->setTimezone(new DateTimeZone('UTC'));

        $output->writeln($T->id_tache);
        $T->update('date_derniere_execution', $datetime->format('Y-m-d H:i:s'), $app['pdo']);

        $output->writeln("[ImportAPIMeteoFranceJson]");
        $adresse = $input->getArgument('adresse');
        $output->writeln("Adresse interrogée : " . $adresse);


        // URL
        if (MF_APPLICATION_ID == null)
            return false;

        $urlAuth = 'https://portail-api.meteofrance.fr/token';

        if (!$token = $this->getToken($urlAuth, $output))
            return false ;


        $curl = curl_init();

        //$adresse = 'https://public-api.meteofrance.fr/public/DPObs/v1/station/horaire?id_station=44117002&format=json' ;

        $header = array("Authorization: Bearer $token");
        curl_setopt_array($curl, array(
            CURLOPT_URL => $adresse, CURLOPT_HTTPHEADER => $header,
            CURLOPT_SSL_VERIFYPEER => false, CURLOPT_RETURNTRANSFER => true));


        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

      //  var_dump($err) ;
       // var_dump($response) ;

        $res = json_decode($response) ;
      //  var_dump($res) ;

        if (isset($res[0]->rr1) and $res[0]->rr1 !== null) {
            $output->writeln('Valeur de pluie horaire : '.$res[0]->rr1.' mm') ;
            $myDate = new DateTime($res[0]->validity_time, new DateTimeZone('UTC'));  
            $myDate->setTimezone(new DateTimeZone($app['TIMEZONE'])) ;
            $valeurs[] = array('date_valeur' => $myDate, 'valeur' => $res[0]->rr1);
        }

        $st = rechercheStationParCodeStation($res[0]->geo_id_insee, $app['pdo']);
        $valeursReleve[] = array('id_point_suivi' => $st['id_point_suivi'], 'valeurs' => $valeurs);

        $infosReleve = array('id_user' => 0, 'date_releve' => new \DateTime('now', new DateTimeZone($app['TIMEZONE'])), 'type_releve' => 'auto', 'type_suivi' => 'valeur', 'id_tache' => $input->getArgument('id_tache'));

        $R = new Releve($app);


        if (!$R->insert($infosReleve, $valeursReleve, $app['pdo'])) {
            $T->traceExecution('echec', date('Y-m-d H:i:s'), $app['pdo']);
            throw new Exception('Echec relevé');
        } else {
            $T->traceExecution('succes', date('Y-m-d H:i:s'), $app['pdo']);
            return true;
        }

    }


    private function getToken($urlAuth, $output)
    {


        $curl = curl_init();

        $idApp = MF_APPLICATION_ID ;
        $header = array("Authorization: Basic $idApp");
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $urlAuth, CURLOPT_HTTPHEADER => $header,
            CURLOPT_SSL_VERIFYPEER => false, CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true, CURLOPT_POSTFIELDS => 'grant_type=client_credentials'));

      
        $response = curl_exec($curl);
        //var_dump($response) ;
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $output->writeln("cURL Error #01: " . $err);
            return false ;
        } else {
            $response = json_decode($response, true);
            if (array_key_exists("access_token", $response)) return $response['access_token'];
            if (array_key_exists("error", $response)) {
                $output->writeln($response["error_description"]);
                $output->writeln("cURL Error #02: Something went wrong! Please contact admin.");
                return false ;
            }
        }
    }
}