<?php

// https://github.com/kendepelchin/silex-base/blob/master/src/Classes/Console/ConsoleCommand.php


use Knp\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportAPIHydrometrieJson extends Command {

    protected function configure() {
        $this
                ->setName('import-api-hydrometrie-json')
                ->setDescription('Collecte des données de l\'API hydrométrie au format json')
                ->setDefinition(
                        new InputDefinition(array(
                    new InputArgument('id_tache', InputArgument::REQUIRED),
                    new InputArgument('adresse', InputArgument::REQUIRED),
                )))

        ; // nice, new line
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $app = $this->getSilexApplication();
        $datetime = new DateTime('now', new DateTimeZone($app['TIMEZONE']));
        $T = new TachePlanifiee($app['pdo'], $input->getArgument('id_tache'));
        
        $date_derniere_execution = new DateTime($T->date_derniere_execution, new DateTimeZone($app['TIMEZONE']));
        $date_derniere_execution->setTimezone(new DateTimeZone('UTC')) ;
                
        $output->writeln($T->id_tache);
        $T->update('date_derniere_execution', $datetime->format('Y-m-d H:i:s'), $app['pdo']);

        $output->writeln("[ImportAPIHydrometrieJson]");
        $adresse = $input->getArgument('adresse');
        // charge les données depuis 2 heures avant le dernier relevé, et dans l'ordre chronologique
        $adresse .= '&sort=asc&date_debut_obs='.$date_derniere_execution->modify('-2 hours')->format('Y-m-d\TH:i:s');
        $output->writeln("Adresse interrogée : " . $adresse);

        $json = json_decode(file_get_contents($adresse));
               
        $valeurs = array();

        foreach ($json->data as $key => $ligne) {

            $myDate = new DateTime($ligne->date_obs, new DateTimeZone('UTC'));  
            $myDate->setTimezone(new DateTimeZone($app['TIMEZONE'])) ;

            $valeurs[] = array('date_valeur' => $myDate, 'valeur' => $ligne->resultat_obs/1000);
        }

      
        $st = rechercheStationParCodeStation($json->data[0]->code_station, $app['pdo']);

        $id_station =  explode(',', substr($T->ids_point_suivi,1,-1)) ;


        $valeursReleve[] = array('id_point_suivi' => $id_station[0], 'valeurs' => $valeurs);

        $infosReleve = array('id_user' => 0, 'date_releve' => new \DateTime('now', new DateTimeZone($app['TIMEZONE'])), 'type_releve' => 'auto', 'type_suivi' => 'valeur', 'id_tache' => $input->getArgument('id_tache'));

        $R = new Releve($app);


        if (!$R->insert($infosReleve, $valeursReleve, $app['pdo'])) {
            $T->traceExecution('echec', date('Y-m-d H:i:s'), $app['pdo']);
            throw new Exception('Echec relevé');
        } else {
            $T->traceExecution('succes', date('Y-m-d H:i:s'), $app['pdo']);
            return true;
        }
    }

}
