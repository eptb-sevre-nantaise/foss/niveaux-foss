<?php

// https://github.com/kendepelchin/silex-base/blob/master/src/Classes/Console/ConsoleCommand.php


use Knp\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportVerdon extends Command {

    protected function configure() {
        $this
                ->setName('import-verdon')
                ->setDescription('Collecte des données de http://www.cholet.fr/barrages/ pour les barrage du Verdon')
                ->setDefinition(
                        new InputDefinition(array(
                    new InputArgument('id_tache', InputArgument::REQUIRED),
                    new InputArgument('adresse', InputArgument::REQUIRED),
                )))

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $app = $this->getSilexApplication();
        $datetime = new DateTime('now', new DateTimeZone($app['TIMEZONE']));
        $T = new TachePlanifiee($app['pdo'], $input->getArgument('id_tache'));
        $output->writeln($T->id_tache);
        $T->update('date_derniere_execution', $datetime->format('Y-m-d H:i:s'), $app['pdo']);

        $output->writeln("[ImportVerdon]");
        $adresse = $input->getArgument('adresse');
        $output->writeln("Adresse interrogée : " . $adresse);

        /*
         * 
         */

        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTMLFile($adresse);

        $finder = new DOMXPath($dom);


        // VERDON

        $compt = 0;
        $tabValeurs = $dates = array();
        $dateEnForme = $date = null;

        $nodes = $finder->query('//*[@id="verdon_tab"]');

        foreach ($nodes as $node) {

            foreach ($node->childNodes as $ligne) {


                // seulement les lignes contenant de l'info utile
                if (strripos($ligne->textContent, 'à')) {


                    $infos = explode(' ', $ligne->textContent);

                    $tabValeurs[$compt]['valeur'] = str_replace(',', '.', $infos[3]);
                    $dateEnForme = $infos[0] . ' ' . substr($infos[2], 0, -1) . ':00';

                    $date = \DateTime::createFromFormat('d/m/y H:i', $dateEnForme, new DateTimeZone($app['TIMEZONE']));

                    // date UTC
                    //echo $date->format('Y-m-d H:i');
                    // date Paris
                    $date->setTimeZone(new DateTimeZone($app['TIMEZONE']));
                    //echo $date->format('Y-m-d H:i');

                    $tabValeurs[$compt]['date_valeur'] = new \DateTime($date->format('Y-m-d H:i'), new DateTimeZone($app['TIMEZONE']));

                    $dates[$compt] = $tabValeurs[$compt]['date_valeur']->format('Y-m-d H:i');
                    $compt++;
                }
            }
        }

        if (count($date) > 0) {
            array_multisort($dates, SORT_ASC, $tabValeurs);
        }


        $valeursReleve = array('0' => array('id_point_suivi' => 26, 'valeurs' => $tabValeurs));

        $infosReleve = array('id_user' => 0, 'date_releve' => new \DateTime('now', new DateTimeZone($app['TIMEZONE'])), 'type_releve' => 'auto', 'type_suivi' => 'valeur', 'id_tache' => $input->getArgument('id_tache'));

        // var_dump($valeursReleve[0]['valeurs'][0]) ;

        $R = new Releve($app);


        if (!$R->insert($infosReleve, $valeursReleve, $app['pdo'])) {
            $T->traceExecution('echec', date('Y-m-d H:i:s'), $app['pdo']);
            throw new Exception('Echec relevé');
        } else {
            $T->traceExecution('succes', date('Y-m-d H:i:s'), $app['pdo']);
            return true;
        }
    }

}
