<?php

// https://github.com/kendepelchin/silex-base/blob/master/src/Classes/Console/ConsoleCommand.php


use Knp\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportVigicrueJson extends Command {

    protected function configure() {
        $this
                ->setName('import-vigicrue-json')
                ->setDescription('Collecte des données de vigicrues.gouv.fr au format json')
                ->setDefinition(
                        new InputDefinition(array(
                    new InputArgument('id_tache', InputArgument::REQUIRED),
                    new InputArgument('adresse', InputArgument::REQUIRED),
                )))

        ; // nice, new line
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $app = $this->getSilexApplication();
        $datetime = new DateTime('now', new DateTimeZone($app['TIMEZONE']));
        $T = new TachePlanifiee($app['pdo'], $input->getArgument('id_tache'));
        $output->writeln($T->id_tache);
        $T->update('date_derniere_execution', $datetime->format('Y-m-d H:i:s'), $app['pdo']);

        $output->writeln("[ImportVigicrueJson]");
        $adresse = $input->getArgument('adresse');
        $output->writeln("Adresse interrogée : " . $adresse);

        $json = json_decode(file_get_contents($adresse));

        $valeurs = array();

        foreach ( (array) $json->Serie->ObssHydro as $key => $ligne) {

            $myDate = new DateTime();
            $myDate->setTimestamp('@' + round($ligne->DtObsHydro / 1000));

            $valeurs[] = array('date_valeur' => $myDate, 'valeur' => $ligne->ResObsHydro);
        }


        $st = rechercheStationParCodeStation($json->Serie->CdStationHydro, $app['pdo']);


        $valeursReleve[] = array('id_point_suivi' => $st['id_point_suivi'], 'valeurs' => $valeurs);

        $infosReleve = array('id_user' => 0, 'date_releve' => new \DateTime('now', new DateTimeZone($app['TIMEZONE'])), 'type_releve' => 'auto', 'type_suivi' => 'valeur', 'id_tache' => $input->getArgument('id_tache'));

        $R = new Releve($app);


        if (!$R->insert($infosReleve, $valeursReleve, $app['pdo'])) {
            $T->traceExecution('echec', date('Y-m-d H:i:s'), $app['pdo']);
            throw new Exception('Echec relevé');
        } else {
            $T->traceExecution('succes', date('Y-m-d H:i:s'), $app['pdo']);
            return true;
        }
    }

}
