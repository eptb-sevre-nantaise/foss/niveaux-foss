<?php

// https://github.com/kendepelchin/silex-base/blob/master/src/Classes/Console/ConsoleCommand.php


use Knp\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportInfoClimat extends Command {

    protected function configure() {
        $this
                ->setName('import-infoclimat')
                ->setDescription('Collecte des données de infoclimat.fr')
                ->setDefinition(
                        new InputDefinition(array(
                    new InputArgument('id_tache', InputArgument::REQUIRED),
                    new InputArgument('adresse', InputArgument::REQUIRED),
                )))

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $app = $this->getSilexApplication();
        $datetime = new DateTime('now', new DateTimeZone($app['TIMEZONE']));
        $T = new TachePlanifiee($app['pdo'], $input->getArgument('id_tache'));
        $output->writeln($T->id_tache);
        $T->update('date_derniere_execution', $datetime->format('Y-m-d H:i:s'), $app['pdo']);


        $output->writeln("[ImportInfoClimat]");
        $adresse = $input->getArgument('adresse');
        $output->writeln("Adresse interrogée : " . $adresse);

        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTMLFile($adresse);

        $finder = new DOMXPath($dom);

        // ligne du tableau = valeurs
        $compt = 0;
        $tabValeurs = $dates = array();
        $infosDate = $tabInfosDate = $dateEnForme = $date = null;


        // DATES
        $nodes = $finder->query('//table[@id="tableau-releves"]/tbody/tr/td[count(//table[@id="tableau-releves"]/thead/tr/th[.="Heure"]/preceding-sibling::th)+1]/span[@class="tipsy-trigger"]');
        //var_dump($nodesDate) ;


        foreach ($nodes as $node) {

            $infosDate = $node->getAttribute('title');
            $tabInfosDate = explode('<br />', $infosDate);
            $dateEnForme = $tabInfosDate[1] . ' ' . str_replace(array('<b>', '</b>', ' UTC'), '', $tabInfosDate[2]);
            $date = \DateTime::createFromFormat('d/m/Y H\hi', $dateEnForme, new DateTimeZone('UTC'));


            // date Paris
            $date->setTimeZone(new DateTimeZone($app['TIMEZONE']));


            $tabValeurs[$compt]['date_valeur'] = new \DateTime($date->format('Y-m-d H:i'), new DateTimeZone($app['TIMEZONE']));

            $compt++;
        }


        // DONNEES

        $compt = 0;
        $valeur = null;

        $nodes = $finder->query('//table[@id="tableau-releves"]/tbody/tr/td[count(//table[@id="tableau-releves"]/thead/tr/th[.="Pluie"]/preceding-sibling::th)+1]');
        //var_dump($nodesDate) ;
        foreach ($nodes as $node) {

            //var_dump($node->nodeValue);

            if (isset($node->firstChild) and $node->firstChild->textContent != '') {
                $valeur = floatval($node->firstChild->textContent);
                //echo $valeur.'       ' ;
                $tabValeurs[$compt]['valeur'] = $valeur;
                $dates[$compt] = $tabValeurs[$compt]['date_valeur']->format('Y-m-d H:i');
            } else {
                unset($tabValeurs[$compt]);
            }

            $compt++;
        }


        if (count($date) > 0) {
            array_multisort($dates, SORT_ASC, $tabValeurs);
        }

        // attention ids_point_suivi forcément unique
        $valeursReleve = array('0' => array('id_point_suivi' => floatval(substr($T->ids_point_suivi, 1, -1)), 'valeurs' => $tabValeurs));

        $infosReleve = array('id_user' => 0, 'date_releve' => new \DateTime('now', new DateTimeZone($app['TIMEZONE'])), 'type_releve' => 'auto', 'type_suivi' => 'valeur', 'id_tache' => $input->getArgument('id_tache'));

        $R = new Releve($app);

        if (!$R->insert($infosReleve, $valeursReleve, $app['pdo'])) {
            $T->traceExecution('echec', date('Y-m-d H:i:s'), $app['pdo']);
            throw new Exception('Echec relevé');
        } else {
            $T->traceExecution('succes', date('Y-m-d H:i:s'), $app['pdo']);
            return true;
        }
    }

}
