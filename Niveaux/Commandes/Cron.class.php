<?php

use Knp\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Cron extends Command {

    protected function configure() {
        $this
                ->setName('cron')
                ->setDescription('Recherche les tâches à lancer et les lance')

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $app = $this->getSilexApplication();

        $tabT = chargeTachesAExecuter($app['pdo']);
        
      
        foreach ($tabT as $T) {


            // ajouter les nouvelles tâches dans console.php

            $output->writeln('[Lancement de ' . $T['tache'] . ']');
            try {
                if ($T['parametres_ligne_commande'] != null) {
                    $comm = 'php '.PATH.'Niveaux/Console/console.php ' . $T['ligne_commande'] . ' ' . $T['id_tache'] . ' \'' . $T['parametres_ligne_commande'] . '\'';
                } else {
                    $comm = 'php '.PATH.'Niveaux/Console/console.php ' . $T['ligne_commande'] . ' ' . $T['id_tache'];
                }
                $output->writeln($comm);
                exec($comm);
            } catch (Exception $e) {
                $output->writeln('echec');
            }

            $output->writeln('[/ Fin de ' . $T['tache'] . ']');
        }
    }

}
