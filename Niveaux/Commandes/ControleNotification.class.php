<?php

// https://github.com/kendepelchin/silex-base/blob/master/src/Classes/Console/ConsoleCommand.php


use Knp\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ControleNotification extends Command {

    protected function configure() {
        $this
                ->setName('controle-notification')
                ->setDescription('Vérification si des notifications doivent être envoyées, et envoi')
                ->setDefinition(
                        new InputDefinition(array(
                            new InputArgument('id_tache', InputArgument::REQUIRED)
                        )))

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $app = $this->getSilexApplication();
        $datetime = new DateTime('now', new DateTimeZone($app['TIMEZONE']));
        $T = new TachePlanifiee($app['pdo'], $input->getArgument('id_tache'));
        //$output->writeln($T->id_tache);


        $output->writeln("[ControleNotification]");
        if (ControleNotification::rechercheNotifications($app, $output)) {
            $T->traceExecution('succes', date('Y-m-d H:i:s'), $app['pdo']);
            $T->update('date_derniere_execution', $datetime->format('Y-m-d H:i:s'), $app['pdo']);
            $output->writeln("[/ ControleNotification]");
            return true;
        } else {
            $T->traceExecution('echec', date('Y-m-d H:i:s'), $app['pdo']);
            $T->update('date_derniere_execution', $datetime->format('Y-m-d H:i:s'), $app['pdo']);
            $output->writeln("[/ ControleNotification]");
            throw new Exception('Echec controle notif');
        }
    }

    /*
     * recherche si les valeurs insérées depuis la dernière recherche de notifications
     * ont dépassé le seuil d'une ou plusieurs notifications et liste ces notifications
     * limite la recherche aux dernières 24 heures
     */

    private function rechercheNotifications($app, OutputInterface $output) {

        $pdo = $app['pdo'];
        $output->writeln("Recheche des valeurs relevées dépassant les seuils depuis " . ControleNotification::dernierControleNotification($pdo));

        $now = new DateTime('now', new DateTimeZone($app['TIMEZONE']));
        $dernierControle = new \DateTime(ControleNotification::dernierControleNotification($pdo), new DateTimeZone($app['TIMEZONE']));


        // toutes les nouvelles valeurs qui pourraient potentiellement déclencher une notification

        $query = 'select v.valeur,v.id_point_suivi,v.date_valeur,v.id_releve,v.date_releve,n.id_notification,n.seuil,n.montant_descendant,n.fonction,n.fonction_parametres '
                . 'from ' . SCHEMA . '.valeur_infos v '
                . 'inner join ' . SCHEMA . '.notification n on n.id_point_suivi = v.id_point_suivi '
                . 'where v.date_releve > \'' . $dernierControle->format('Y-m-d H:i:s') . '\' '
                . 'and v.date_valeur > \'' . $now->modify('-1 day')->format('Y-m-d H:i:s') . '\' '
                . 'and n.active = 1 '
                . 'order by v.date_valeur asc';

        //$output->writeln('Query ' . $query);

        $qry = $pdo->prepare($query);


        $qry->execute();
        $output->writeln($qry->rowCount() . " valeurs trouvées");

        while ($v = $qry->fetchObject()) {

            // comparaison du seuil et de la valeur, selon le type de fonction de la notification
            $fonction = $v->fonction;
            $parametres = json_decode($v->fonction_parametres);

            $res = verifieFonction($app, ['valeur' => $v->valeur, 'date_valeur' => $v->date_valeur, 'seuil' => $v->seuil, 'montant_descendant' => $v->montant_descendant, 'id_point_suivi' => $v->id_point_suivi, 'fonction' => $v->fonction, 'parametres' => json_decode($v->fonction_parametres, true)]);
            $output->writeln($res['message']);

            if ($res['result'] != false) {
                $output->writeln("Envoi Notification " . $v->id_notification);
                $N = new Notification($pdo, $v->id_notification);
                $N->envoie($v->id_releve, $res['valeur_actuelle'], new DateTime($v->date_valeur), ['valeur' => $res['derniere_valeur'], 'date_valeur' => $res['date_derniere_valeur']], $app);
            } else {
                $output->writeln("Pas de notification");
            }
        }
        return true;
    }

    /*
     * date de la dernière exécution de la tâche
     */

    private function dernierControleNotification($pdo) {


        $qry = $pdo->prepare('select date_derniere_execution from ' . SCHEMA . '.tache_planifiee '
                . 'where ligne_commande = \'controle-notification\' ');
        $qry->execute();

        if ($qry->rowCount() > 0) {
            $res = $qry->fetchObject();
            return $res->date_derniere_execution;
        } else {
            return '2000-01-01 00:00:00+02';
        }
    }

}
