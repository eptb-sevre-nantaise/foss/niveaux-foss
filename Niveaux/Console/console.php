#!/usr/bin/env php
<?php

set_time_limit(0);

require_once dirname(__DIR__) ."/CommonConf.php";
$app = require_once dirname(__DIR__) ."/ConsoleConf.php";

$console = $app["console"];
$console->add(new ImportVigicrueJson());
$console->add(new ImportAPIHydrometrieJson());
$console->add(new ImportInfoClimat());
$console->add(new ImportAPIMeteoFranceJson());
$console->add(new ControleNotification());
$console->add(new Cron());
$console->run();