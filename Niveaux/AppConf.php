<?php

/*
 * partie de la conf spécifique à l'application web
 */


// TWIG
$app->register(new Silex\Provider\HttpFragmentServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/../views'
));

// TRANSLATION
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale_fallbacks' => array('fr'),
));

// VALIDATOR
use Symfony\Component\Validator\Constraints as Assert;

$app->register(new Silex\Provider\ValidatorServiceProvider());

// FORM
use Silex\Provider\FormServiceProvider;

$app->register(new FormServiceProvider());


// SESSION & COOKIES
$app->register(new Silex\Provider\SessionServiceProvider());
// session par defaut sur xx heures
$app['session.storage.options'] = [
    'cookie_lifetime' => (3600 * 24 * 7)
];

use Symfony\Component\HttpFoundation\Cookie;

// URL MANAGER
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

// REQUEST & RESPONSE & FLASH
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

// SECURITY

$app['security.firewalls'] = array(
    // 'security' => $app['debug'] ? false : true,

    'secured' => array(
        'pattern' => '^/',
        'anonymous' => true,
        'form' => array('login_path' => '/login', 'check_path' => '/control', 'default_target_path' => '/control', 'always_use_default_target_path' => true, 'csrf_token_generator' => 'security.csrf.token_manager'),
        'logout' => array('logout_path' => '/logout', 'invalidate_session' => true, 'target' => '/'),
        'remember_me' => array(
            'key' => REMEMBERKEY,
            'always_remember_me' => true,
        ),
        'users' => function () use ($app) {
            return new UserProviderNiveaux($app['pdo']);
        },
    ),
);

$app->register(new Silex\Provider\SecurityServiceProvider(), array(
    'security.firewalls' => $app['security.firewalls']
));
$app['security.access_rules'] = array(
    array('^/admin', 'ROLE_ADMIN')
);


$app->register(new Silex\Provider\RememberMeServiceProvider(), array('lifetime' => 3600 * 24 * 7));
$app['security.default_encoder'] = function ($app) {
    return $app['security.encoder.bcrypt'];
};

// SMS OVH
use \Ovh\Sms\SmsApi;

