<?php

/*
 * Description of Utilisateur
 *
 * @author srenou
 */

class Utilisateur {

    public function __construct($pdo, $username = null) {

        if ($username !== null) {

            $qry = $pdo->prepare('select id_user,username,password,nom,prenom,mail,telephone,cond_util_validees,date_valid_cond_util,roles,bdc_id_contact,bdc_id_structure from ' . SCHEMA . '.utilisateur where username = :username');
            $qry->bindParam(':username', $username, PDO::PARAM_STR);
            $qry->execute();
            $res = $qry->fetch(PDO::FETCH_ASSOC);
            foreach ($res as $key => $value) {
                $this->$key = $value;
            }
            $this->chargePointSuiviFavori($pdo);
        }
    }

    public function insert($pdo, $request) {

        $pp = prepInsertQuery($request);

        $qry = $pdo->prepare("insert into " . SCHEMA . ".utilisateur (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ")");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $pdo, $pp['tabD']);
    }

    public function update($pdo, $request) {


        $pp = prepUpdateQuery($request, 'id_user');
        $qry = $pdo->prepare("update " . SCHEMA . ".utilisateur set " . $pp['req'] . " where id_user = :id_user");


        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);           
        }

        return returnResultQuery($qry, $pdo, $pp['tabD']);
    }

    public function updatecol($col, $val, $pdo) {

        $req = 'update ' . SCHEMA . '.utilisateur set ' . $col . ' = :val where id_user = :id_user';
        $qry = $pdo->prepare($req);
        $qry->bindParam(':val', $val);
        $qry->bindValue(':id_user', $this->id_user, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function chargeDroits($pdo) {

        $droits = array();
        $qry = $pdo->prepare("select * from " . SCHEMA . ".l_user_point_suivi where id_user = :id_user");
        $qry->bindValue(':id_user', $this->id_user, PDO::PARAM_INT);
        $qry->execute();
        while ($res = $qry->fetchObject()) {
            $droits[$res->droit] = $res->ids_points_suivi;
        }

        return $droits;
    }

    public function metAJourDroits($pdo, $request) {

        if ($this->supprimeDroits($pdo)) {


            $qry = $pdo->prepare("insert into " . SCHEMA . ".l_user_point_suivi (id_user,droit,ids_points_suivi) VALUES (:id_user,:droit,:ids_points_suivi)");
            $qry->bindValue(':id_user', $this->id_user, PDO::PARAM_INT);
            $qry->bindValue(':droit', 'ROLE_LECTEUR', PDO::PARAM_STR);
            $qry->bindValue(':ids_points_suivi', $request['ROLE_LECTEUR']);


            $qry2 = $pdo->prepare("insert into " . SCHEMA . ".l_user_point_suivi (id_user,droit,ids_points_suivi) VALUES (:id_user,:droit,:ids_points_suivi)");
            $qry2->bindValue(':id_user', $this->id_user, PDO::PARAM_INT);
            $qry2->bindValue(':droit', 'ROLE_CONTRIB', PDO::PARAM_STR);
            $qry2->bindValue(':ids_points_suivi', $request['ROLE_CONTRIB'], PDO::PARAM_STR);

            $qry3 = $pdo->prepare("insert into " . SCHEMA . ".l_user_point_suivi (id_user,droit,ids_points_suivi) VALUES (:id_user,:droit,:ids_points_suivi)");
            $qry3->bindValue(':id_user', $this->id_user, PDO::PARAM_INT);
            $qry3->bindValue(':droit', 'ROLE_GESTIONNAIRE', PDO::PARAM_STR);
            $qry3->bindValue(':ids_points_suivi', $request['ROLE_GESTIONNAIRE'], PDO::PARAM_STR);


            if ($qry->execute() and $qry2->execute() and $qry3->execute()) {
                return true;
            } else {
                return false;
            }
        }
    }

    private function supprimeDroits($pdo) {

        $qry = $pdo->prepare('delete from  ' . SCHEMA . '.l_user_point_suivi where id_user = :id_user');
        $qry->bindValue(':id_user', $this->id_user, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function aAcces($droit, $id_point_suivi, $pdo) {

        $qry = $pdo->prepare('select * from ' . SCHEMA . '.l_user_point_suivi where id_user = :id_user and droit = :droit and (:id_point_suivi = ANY(ids_points_suivi) or ids_points_suivi = \'{0}\')');
        $qry->bindValue(':id_user', $this->id_user, PDO::PARAM_INT);
        $qry->bindParam('droit', $droit, PDO::PARAM_STR);
        $qry->bindParam('id_point_suivi', $id_point_suivi, PDO::PARAM_INT);
        $qry->execute();
        if ($qry->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function chargeNotifications($pdo) {

        $qry = $pdo->prepare('select * from ' . SCHEMA . '.notification_infos where id_user = :id_user');
        $qry->bindParam('id_user', $this->id_user);
        $qry->execute();
        return $qry->fetchAll(PDO::FETCH_ASSOC);
    }

    public function nombreEnvoisNotifSMS($duree, $app) {

        $pdo = $app['pdo'];
        $qry = $pdo->prepare('select id_notification from ' . SCHEMA . '.notification_envoi_infos where username = :username and type_notification = \'sms\' and date_envoi_notification > :dt and statut = \'succes\' ');

        $qry->bindParam(':username', $this->username);
        $now = new DateTime('now', new DateTimeZone($app['TIMEZONE']));
        $depuis = $now->modify('-' . $duree)->format('Y-m-d H:i:s');
        // echo $depuis ;
        $qry->bindParam(':dt', $depuis);
        $qry->execute();
        // var_dump($pdo->errorInfo()) ;
        return $qry->rowCount();
    }

    public function ajoutePointSuiviFavori($idp, $pdo) {

        $qry = $pdo->prepare('insert into ' . SCHEMA . '.l_user_point_suivi_favori (id_point_suivi,id_user) VALUES (:idp,:idu)');
        $qry->bindParam(':idp', $idp, PDO::PARAM_INT);
        $qry->bindParam(':idu', $this->id_user, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function supprimePointSuiviFavori($idp, $pdo) {

        $qry = $pdo->prepare('delete from  ' . SCHEMA . '.l_user_point_suivi_favori where id_point_suivi = :idp and id_user= :idu');
        $qry->bindParam(':idp', $idp, PDO::PARAM_INT);
        $qry->bindParam(':idu', $this->id_user, PDO::PARAM_INT);
        return $qry->execute();
    }

    public function chargePointSuiviFavori($pdo) {

        $this->tabFavoris = array();
        $qry = $pdo->prepare('select * from  ' . SCHEMA . '.l_user_point_suivi_favori where id_user= :idu');
        $qry->bindParam(':idu', $this->id_user, PDO::PARAM_INT);
        $qry->execute();
        while ($res = $qry->fetchObject()) {
            $this->tabFavoris[$res->id_point_suivi] = true;
        }
    }

    public function estPointSuiviFavori($idp, $pdo) {

        $this->tabFavoris = array();

        $qry = $pdo->prepare('select * from  ' . SCHEMA . '.l_user_point_suivi_favori where  id_point_suivi = :idp and id_user= :idu');
        $qry->bindParam(':idp', $idp, PDO::PARAM_INT);
        $qry->bindParam(':idu', $this->id_user, PDO::PARAM_INT);
        $qry->execute();
        return $qry->rowCount();
    }

    public function traceConnexion($pdo) {

        $qry = $pdo->prepare('insert into ' . SCHEMA . '.utilisateur_connexion (id_user) VALUES (:id)');
        $qry->bindParam(':id', $this->id_user, PDO::PARAM_INT);        
        return $qry->execute();
    }

}
