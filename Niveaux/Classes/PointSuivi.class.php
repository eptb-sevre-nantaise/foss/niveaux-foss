<?php

/**
 * Description of PointSuivi
 *
 * @author srenou
 */
class PointSuivi {

    public function __construct($pdo, $id = null, $alias = null) {


        if ($id !== null or $alias !== null) {

            $condId = $condAlias = null;

            if ($id !== null)
                $condId = ' and id_point_suivi = :id';
            if ($alias !== null)
                $condAlias = ' and alias = :alias';

            $qry = $pdo->prepare('select * from ' . SCHEMA . '.point_suivi_infos where 1=1 ' . $condId . $condAlias);
            if ($alias !== null)
                $qry->bindParam(':alias', $alias, PDO::PARAM_STR);
            if ($id !== null)
                $qry->bindParam(':id', $id, PDO::PARAM_INT);
            $qry->execute();
            if ($qry->rowCount() === 1) {
                $res = $qry->fetch(PDO::FETCH_ASSOC);
                foreach ($res as $key => $value) {
                    $this->$key = $value;
                }
                $this->tabSeuils = array();
            }
        }
    }

    public function insert($pdo, $request) {

        $pp = prepInsertQuery($request);

        $qry = $pdo->prepare("insert into " . SCHEMA . ".point_suivi (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ")");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $pdo, $pp['tabD']);
    }

    public function update($pdo, $request) {

        $pp = prepUpdateQuery($request, 'id_point_suivi');
        $qry = $pdo->prepare("update " . SCHEMA . ".point_suivi set " . $pp['req'] . " where id_point_suivi = :id_point_suivi");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
            echo $key . $val . '<br>';
        }

        return returnResultQuery($qry, $pdo, $pp['tabD']);
    }

    /**
     * @param string type_releve : manuel ou auto
     * @param string type_suivi : valeur ou seuil
     */
    public function chargeValeurs($pdo, $date_debut, $date_fin = null, $type_releve = null, $type_suivi = null, $heures_slmt = 0) {

        $condDateFin = $condTypeReleve = $condTypeSuivi = $condHeureSlmt = null;

        $condDateDebut = ' and date_valeur >= :date_debut ';
        if ($date_fin !== null)
            $condDateFin = ' and date_valeur <= :date_fin ';
        if ($type_releve !== null)
            $condTypeReleve = ' and type_releve = :type_releve ';
        if ($type_suivi !== null)
            $condTypeSuivi = ' and type_suivi = :type_suivi ';
        if ($heures_slmt == '1')
            $condHeureSlmt = ' and extract(\'minute\' from date_valeur) =  0 ';


        $i = 'select * from ' . SCHEMA . '.valeur_infos where id_point_suivi = :id ' . $condDateDebut . $condDateFin . $condTypeReleve . $condTypeSuivi . $condHeureSlmt . ' order by date_valeur asc';

        $qry = $pdo->prepare($i);

        $qry->bindParam(':id', $this->id_point_suivi, PDO::PARAM_STR);
        $dt = date_format($date_debut, 'Y-m-d H:i:s');

        $qry->bindParam(':date_debut', $dt, PDO::PARAM_STR);
        if ($date_fin !== null)
            $qry->bindParam(':date_fin', $date_fin, PDO::PARAM_STR);
        if ($type_releve !== null)
            $qry->bindParam(':type_releve', $type_releve, PDO::PARAM_STR);
        if ($type_suivi !== null)
            $qry->bindParam(':type_suivi', $type_suivi, PDO::PARAM_STR);

        $qry->execute();
        return $qry->fetchAll(PDO::FETCH_ASSOC);
    }

    /*
     * recherche des seuils 
     * tous ou en dessous du max + 1m
     * 
     */

    public function chargeSeuils($pdo, $max = null) {
        $condM = null;
        if ($max != null) {
            $condM = ' and valeur <= :max';
        }
        $qry = $pdo->prepare('select * from ' . SCHEMA . '.seuil where id_point_suivi = :id ' . $condM . ' order by valeur');
        $qry->bindParam(':id', $this->id_point_suivi, PDO::PARAM_INT);
        if ($max != null) {
            $max = $max + 1;
            $qry->bindParam(':max', $max, PDO::PARAM_INT);
        }
        $qry->execute();
        $this->tabSeuils = $qry->fetchAll(PDO::FETCH_ASSOC);
    }

}
