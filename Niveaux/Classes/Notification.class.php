<?php

/**
 * Description of Notification
 *
 * @author srenou
 */
class Notification {

    public function __construct($pdo, $id = null) {

        if ($id !== null) {

            $qry = $pdo->prepare('select * from ' . SCHEMA . '.notification_infos where id_notification = :id');
            $qry->bindParam(':id', $id, PDO::PARAM_INT);
            $qry->execute();
            $res = $qry->fetch(PDO::FETCH_ASSOC);
            foreach ($res as $key => $value) {
                $this->$key = $value;
            }
            if ($this->fonction == 'valeurActuelleBrute') {
                $this->fonction_notification = 'valeurActuelleBrute';
            }
            if ($this->fonction == 'moyenneSurDuree' and $this->fonction_parametres == '{"duree":"-3 days"}') {
                $this->fonction_notification = 'moyenneSurDuree3Jours';
            }
            return true;
        }
        return false;
    }

    public function insert($pdo, $request) {

        $pp = prepInsertQuery($request);

        $qry = $pdo->prepare("insert into " . SCHEMA . ".notification (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ")");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $pdo, $pp['tabD']);
    }

    public function update($pdo, $request) {


        $pp = prepUpdateQuery($request, 'id_notification');
        $qry = $pdo->prepare("update " . SCHEMA . ".notification set " . $pp['req'] . " where id_notification = :id_notification");


        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
            echo $key . $val . '<br>';
        }

        return returnResultQuery($qry, $pdo, $pp['tabD']);
    }

    public function delete($pdo) {

        $qry = $pdo->prepare('delete from ' . SCHEMA . '.notification where  id_notification = :id_notification');
        $qry->bindParam(':id_notification', $this->id_notification, PDO::PARAM_INT);
        return $qry->execute();
    }

    /*
     * function envoie
     * 
     * @param integer id_releve 
     * @param string valeur 
     * @param DateTime date_valeur,
     * @param array Dern
     * @param pdo
     */

    public function envoie($id_releve, $valeur, $date_valeur, $Dern, $app) {

        $pdo = $app['pdo'];
        $R = new Releve($app, $id_releve);
        $dtdern = new DateTime($Dern['date_valeur'], new DateTimeZone($app['TIMEZONE']));

        if ($this->type_notification === 'mail') {

            $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
                        <html xmlns="http://www.w3.org/1999/xhtml">
                        <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title>Notification ' . $app['BRAND'] . '</title>
                        <style>
                        body {
                            font-family:arial,sans-serif;
                            }
                        </style>
                        </head>
                        <body>';
            $message .= '<h2>Notification ' . $app['BRAND'] . ' ' . $app['BRANDSTRUCTURE'] . '</h2>';
            $message .= '<h1>Point de suivi ' . $this->point_suivi . ' / ' . $this->element_mesure . ' : ' . $valeur . ' ' . $this->unite . ' (' . $app['FONCTIONS_NOTIFICATIONS'][$this->fonction.' '.$this->fonction_parametres] . ') à ' . $date_valeur->format('H:i \l\e d/m/Y') . ' (' . $this->montant_descendant . ')</h1>';
            $message .= '<p>Cette notification a été déclenchée automatiquement suite au dernier relevé effetué sur la station de ' . $this->point_suivi . '</p>';
            $message .= '<p>Informations complémentaires : </p>';
            $message .= '<ul>';
            $message .= '<li>Nouvelle valeur de ' . $valeur . ' ' . $this->unite . ' (' . $app['FONCTIONS_NOTIFICATIONS'][$this->fonction.' '.$this->fonction_parametres] . ') issue du relevé ' . $R->type_releve . ' du ' . $R->datetime_releve->format('d/m/Y \à H:i') . ' effectué par ' . $R->prenom . ' ' . $R->nom . '</li>';
            $message .= '<li>Valeur précédente : ' . $Dern['valeur'] . ' ' . $this->unite . ' (' . $app['FONCTIONS_NOTIFICATIONS'][$this->fonction.' '.$this->fonction_parametres] . ') le ' . $dtdern->format('d/m/Y \à H:i') . '</li>';
            $message .= '</ul>';
            $message .= '<p>Paramétrage de votre notification : </p>';
            $message .= '<ul>';
            $message .= '<li>' . $this->element_mesure . ' de déclenchement : ' . $this->seuil . ' ' . $this->unite . ' (' . $this->montant_descendant . ')</li>';
            $message .= '</ul>';
            $message .= '</body></html>';

            $object = '' . $this->point_suivi . ' / ' . $this->type_point . ' à ' . $date_valeur->format('H:i \l\e d/m/Y') . ' : ' . $valeur;
            $senderMail = $app['MAILEXP'];
            $sender = $app['BRAND'] . ' ' . $app['BRANDSTRUCTURE'];

            if (envoieMail($this->mail, $senderMail, $sender, $message, $object)) {
                $this->traceEnvoi($id_releve, 'succes', array('dest' => $this->mail, 'sender-mail' => $senderMail, 'sender' => $sender, 'message' => $message, 'object' => $object), $valeur, $date_valeur->format('Y-m-d H:i:s'), $pdo);
                return true;
            } else {
                $this->traceEnvoi($id_releve, 'echec', array($this->mail, $senderMail, $sender, $message, $object), $valeur, $date_valeur->format('Y-m-d H:i:s'), $pdo);
                return false;
            }
        }

        if ($this->type_notification === 'sms') {

            if ($this->telephone != null) {

                $message = $this->point_suivi . ' : ' . $this->element_mesure . ' de ' . $valeur . ' ' . $this->unite . ' à (' . $app['FONCTIONS_NOTIFICATIONS'][$this->fonction.' '.$this->fonction_parametres] . ')' . $date_valeur->format('H:i \l\e d/m/Y') . ' (' . $this->montant_descendant . '). Nouvelle valeur issue du relevé ' . $R->type_releve . ' du ' . $R->datetime_releve->format('d/m/Y \à H:i') . ' effectué par ' . $R->prenom . ' ' . $R->nom . '.';

                if (envoiSMS2($this->username, $this->telephone, $message, $app)) {

                    $this->traceEnvoi($id_releve, 'succes', array('dest' => $this->telephone, 'message' => $message), $valeur, $date_valeur->format('Y-m-d H:i:s'), $pdo);
                    return true;
                } else {
                    $this->traceEnvoi($id_releve, 'echec', array('dest' => $this->telephone, 'message' => $message), $valeur, $date_valeur->format('Y-m-d H:i:s'), $pdo);
                    return false;
                }
            }
        }
    }

    private function traceEnvoi($id_releve, $statut, $contenu, $valeur, $date_valeur, $pdo) {

        $qry = $pdo->prepare('insert into ' . SCHEMA . '.notification_envoi (id_notification,id_releve,statut,contenu_notification,releve_valeur,releve_valeur_date) VALUES (:id_notification,:id_releve,:statut,:contenu,:releve_valeur,to_timestamp(:releve_valeur_date, \'yyyy-mm-dd hh24:mi:ss\'))');
        $qry->bindParam(':id_notification', $this->id_notification, PDO::PARAM_INT);
        $qry->bindParam(':id_releve', $id_releve, PDO::PARAM_INT);
        $qry->bindParam(':statut', $statut, PDO::PARAM_STR);
        $cont = json_encode($contenu);
        $qry->bindParam(':contenu', $cont, PDO::PARAM_STR);
        $qry->bindParam(':releve_valeur', $valeur, PDO::PARAM_STR);
        $qry->bindParam(':releve_valeur_date', $date_valeur, PDO::PARAM_STR);


        return $qry->execute();
    }

    public function chargeEnvois($pdo, $limit = 5) {

        $qry = $pdo->prepare('select * from ' . SCHEMA . '.notification_envoi_infos where id_notification = ' . $this->id_notification . ' order by date_envoi_notification desc LIMIT ' . $limit);
        $qry->execute();
        return $qry->fetchAll(PDO::FETCH_ASSOC);
    }

    public function desactive($pdo) {

        $qry = $pdo->prepare('update ' . SCHEMA . '.notification set active = 0 where id_notification = :id');
        $qry->bindParam(':id', $this->id_notification, PDO::PARAM_INT);
        return $qry->execute();
    }

}
