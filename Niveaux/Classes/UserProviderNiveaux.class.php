<?php


use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class UserProviderNiveaux implements UserProviderInterface {

    private $pdo;

    public function __construct($pdo) {
        $this->pdo = $pdo;
    }

    public function loadUserByUsername($username) {
       
        $qry = $this->pdo->prepare('SELECT * FROM ' . SCHEMA . '.utilisateur WHERE username = :username');
        $username_low = strtolower($username);
        $qry->bindParam(':username', $username_low);
        $qry->execute();
        
        if ($qry->rowCount()<1) {          
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }
        $user = $qry->fetch(PDO::FETCH_ASSOC) ;     
        
        $U = new User($user['username'], $user['password'], explode(',', $user['roles']), true, true, true, true, array('prenom'=>$user['prenom'],'nom'=>$user['nom']));         
        return $U;
    }

    public function refreshUser(UserInterface $user) {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class) {
        return $class === 'Symfony\Component\Security\Core\User\User';
    }

}
