<?php

/**
 * Description of TachePlanifiee
 *
 * @author srenou
 */
class TachePlanifiee {

    public function __construct($pdo, $id = null) {


        if ($id !== null) {
            $qry = $pdo->prepare('select * from ' . SCHEMA . '.tache_planifiee where id_tache = :id');
            $qry->bindParam(':id', $id, PDO::PARAM_INT);
            $qry->execute();
            $res = $qry->fetch(PDO::FETCH_ASSOC);
            foreach ($res as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    public function update($col, $val, $pdo) {

        $req = 'update ' . SCHEMA . '.tache_planifiee set ' . $col . ' = :val where id_tache =:id';
        $qry = $pdo->prepare($req);
        $qry->bindParam(':val', $val);
        $qry->bindParam(':id', $this->id_tache);
        return $qry->execute();
    }

    public function traceExecution($statut, $date_execution, $pdo) {


        $this->update('statut_derniere_execution', $statut, $pdo);


        $qry = $pdo->prepare('insert into ' . SCHEMA . '.tache_executee (statut,date_execution,id_tache) VALUES (:st,:dt,:id)');
        $qry->bindParam(':id', $this->id_tache, PDO::PARAM_INT);
        $qry->bindParam(':st', $statut);
        $qry->bindParam(':dt', $date_execution);
        return $qry->execute();
      
    }

}
