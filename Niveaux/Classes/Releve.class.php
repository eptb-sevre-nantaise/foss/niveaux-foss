<?php

/**
 * Description of Releve
 *
 * @author srenou
 */
class Releve {

    public function __construct($app, $id = null) {

        $pdo = $app['pdo'];

        if ($id !== null) {

            $qry = $pdo->prepare('select * from ' . SCHEMA . '.releve_infos where id_releve = :id');
            $qry->bindParam(':id', $id, PDO::PARAM_INT);
            $qry->execute();
            $res = $qry->fetch(PDO::FETCH_ASSOC);
            foreach ($res as $key => $value) {
                $this->$key = $value;
            }

            $this->datetime_releve = new DateTime($this->date_releve, new DateTimeZone($app['TIMEZONE']));
        }
    }

    /**
     * insert
     * 
     * @param array $infosReleve sur le modèle ['id_user'],['date_releve'],['type_releve'],['type_suivi']
     * @param array valeursReleve sur le modèle ('0'=> array('id_point_suivi'=>xxx, 'valeurs'=>array('date_valeur'=> Y-m-d H:i, 'valeur' => valeur )))
     */
    public function insert($infosReleve, $valeursReleve, $pdo) {


        $erreurs = 0;

        $qry = $pdo->prepare('insert into ' . SCHEMA . '.releve (id_user,type_releve,type_suivi,id_tache) VALUES (:id_user,:type_releve,:type_suivi,:id_tache) RETURNING id_releve as id_releve');


        $qry->bindParam(':id_user', $infosReleve['id_user'], PDO::PARAM_INT);
        $qry->bindParam(':type_releve', $infosReleve['type_releve'], PDO::PARAM_STR);
        $qry->bindParam(':type_suivi', $infosReleve['type_suivi'], PDO::PARAM_STR);
        $qry->bindParam(':id_tache', $infosReleve['id_tache'], PDO::PARAM_INT);
        if ($qry->execute()) {
            $r = $qry->fetchObject();

            // boucle sur les stations du tableau de valeur
            foreach ($valeursReleve as $v) {

                $compt = 0;

                if (is_array($v['valeurs'])) {
                    foreach ((array) $v['valeurs'] as $valeur) {


                        // VALEUR N'EST PAS UN DOUBLON, N EST PAS VIDE et EST NUMERIQUE
                        if (!estUnDoublon($valeur['valeur'], $v['id_point_suivi'], $valeur['date_valeur']->format('Y-m-d H:i:s'), $infosReleve['type_releve'], $infosReleve['type_suivi'], $pdo) and !estVide($valeur['valeur']) and is_numeric($valeur['valeur'])) {


                            // A DEJA UNE INFO MAIS DE VALEUR DIFFERENTE

                            if (aUneValeurDifferente($valeur['valeur'], $v['id_point_suivi'], $valeur['date_valeur']->format('Y-m-d H:i:s'), $infosReleve['type_releve'], $infosReleve['type_suivi'], $pdo)) {


                                $V = valeurActuelleSiDifferente($valeur['valeur'], $v['id_point_suivi'], $valeur['date_valeur']->format('Y-m-d H:i:s'), $infosReleve['type_releve'], $infosReleve['type_suivi'], $pdo);
                                if ($compt === 0) {

                                    $compt++;
                                }


                                // UPDATE

                                $qry2 = $pdo->prepare('update ' . SCHEMA . '.releve_valeur SET id_releve = :id_releve, valeur = :valeur where date_valeur = to_timestamp(:date_valeur, \'yyyy-mm-dd hh24:mi:ss\') and id_point_suivi = :id_point_suivi');
                                $qry2->bindParam(':id_releve', $r->id_releve, PDO::PARAM_INT);
                                $qry2->bindParam(':valeur', $valeur['valeur'], PDO::PARAM_STR);
                                $date_v = $valeur['date_valeur']->format('Y-m-d H:i:s');
                                $qry2->bindParam(':date_valeur', $date_v, PDO::PARAM_STR);
                                $qry2->bindParam(':id_point_suivi', $v['id_point_suivi'], PDO::PARAM_INT);
                            }

                            // NOUVELLE VALEUR
                            else {


                                // INSERT

                                $qry2 = $pdo->prepare('insert into ' . SCHEMA . '.releve_valeur (id_releve,valeur,date_valeur,id_point_suivi) VALUES (:id_releve,:valeur,to_timestamp(:date_valeur, \'yyyy-mm-dd hh24:mi:ss\'),:id_point_suivi)');
                                $qry2->bindParam(':id_releve', $r->id_releve, PDO::PARAM_INT);
                                $qry2->bindParam(':valeur', $valeur['valeur'], PDO::PARAM_STR);
                                $date_v = $valeur['date_valeur']->format('Y-m-d H:i:s');
                                $qry2->bindParam(':date_valeur', $date_v, PDO::PARAM_STR);
                                $qry2->bindParam(':id_point_suivi', $v['id_point_suivi'], PDO::PARAM_INT);
                            }

                            if (!$qry2->execute()) {

                                $erreurs++;
                            } else {

                                // verifieNotifications($r->id_releve, $v['id_point_suivi'], $valeur['valeur'], $valeur['date_valeur'], $pdo);
                            }
                        } else {

                            // echo 'valeur KO' ;
                        }
                    }
                }
            }
        } else {

            die('erreur');
        }

        if ($erreurs === 0) {
            return true;
        } else {
            return false;
        }
    }

}
