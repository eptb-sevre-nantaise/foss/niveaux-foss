<?php

/**
 * Description of Seuil
 *
 * @author srenou
 */
class Seuil {

    public function __construct($pdo, $id = null) {

        if ($id !== null) {

            $qry = $pdo->prepare('select * from ' . SCHEMA . '.seuil where id_seuil = :id');

            $qry->bindParam(':id', $id, PDO::PARAM_INT);
            $qry->execute();
            if ($qry->rowCount() === 1) {
                $res = $qry->fetch(PDO::FETCH_ASSOC);
                foreach ($res as $key => $value) {
                    $this->$key = $value;
                }
            }
        }
    }

    public function insert($pdo, $request) {

        $pp = prepInsertQuery($request);

        $qry = $pdo->prepare("insert into " . SCHEMA . ".seuil (" . $pp['reqFields'] . ") VALUES (" . $pp['reqParams'] . ")");

        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
        }

        return returnResultQuery($qry, $pdo, $pp['tabD']);
    }

    public function update($pdo, $request) {


        $pp = prepUpdateQuery($request, 'id_seuil');
        $qry = $pdo->prepare("update " . SCHEMA . ".seuil set " . $pp['req'] . " where id_seuil = :id_seuil");


        foreach ($pp['tabD'] as $key => $val) {
            $qry->bindValue($key, $val);
            echo $key . $val . '<br>';
        }

        return returnResultQuery($qry, $pdo, $pp['tabD']);
    }

    public function delete($pdo) {

        $qry = $pdo->prepare('delete from ' . SCHEMA . '.seuil where  id_seuil = :id_seuil');
        $qry->bindParam(':id_seuil', $this->id_seuil, PDO::PARAM_INT);
        return $qry->execute();
    }

}
