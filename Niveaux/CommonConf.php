<?php

/*
 * Configuration de silex
 * Partie commune à l'appli web et à la console
 */


// Utils
require_once(dirname(__DIR__) . '/utils/conf.php');
require_once(dirname(__DIR__) . '/utils/utils.php');


// Autoload des vendors
require_once __DIR__ . '/../vendor/autoload.php';

// SILEX
$app = new Silex\Application();
$app['debug'] = DEBUG;

// PDO
use Csanquer\Silex\PdoServiceProvider\Provider\PDOServiceProvider;

$app->register(
// you can customize services and options prefix with the provider first argument (default = 'pdo')
        new PDOServiceProvider('pdo'), array(
    'pdo.server' => array(
// PDO driver to use among : mysql, pgsql , oracle, mssql, sqlite, dblib
        'driver' => 'pgsql',
        'host' => DBHOST,
        'dbname' => DBNAME,
        'port' => DBPORT,
        'user' => DBUSER,
        'password' => DBPWD,
    ),
        )
);



// Paramètres Appli
$parametres = chargeParametresAppli($app['pdo']);
foreach ($parametres as $p) {
    $app[$p['alias']] = $p['valeur'];
}

// Paramètres fonctions notifications, pour l'affichage
$app['FONCTIONS_NOTIFICATIONS'] = [
    'valeurActuelleBrute ' => 'valeur brute',
    'moyenneSurDuree {"duree":"-3 days"}' => 'moyenne sur 3 jours'
];

// Classes NIVEAUX
spl_autoload_register(function ($class) use ($app) {
    $file = PATH . 'Niveaux/Classes/' . str_replace('\\', '/', $class) . '.class.php';
    //echo $file ;
    if (file_exists($file)) {
        require $file;
    }
});


