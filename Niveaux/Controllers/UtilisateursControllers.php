<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

$app->get('/admin/utilisateurs', function() use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_ADMIN') and $app['session']->get('U')->cond_util_validees) {
        return $app['twig']->render('utilisateur/utilisateurs.twig', ['utilisateurs' => chargeUtilisateurs($app['pdo'])]);
    } else {
        $app->abort(403);
    }
})->bind('utilisateurs');


$app->match('/admin/utilisateur/{username}', function(Request $request, $username) use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_ADMIN') and $app['session']->get('U')->cond_util_validees) {
        $U = null;
        if ($username !== 'nouveau') {
            $Util = new Utilisateur($app['pdo'], $username);
            $U = get_object_vars($Util);
        }



        $form = $app['form.factory']->createBuilder('form', $U)
                ->add('username', 'text', array('label' => 'Nom d\'utilisateur (sur le modèle "pnom")', 'constraints' => new Assert\NotBlank()))
                ->add('prenom', 'text', array('constraints' => new Assert\NotBlank()))
                ->add('nom', 'text', array('constraints' => new Assert\NotBlank()))
                ->add('cond_util_validees', 'choice', array('choices' => array('1' => 'Oui', '0' => 'Non'), 'label' => 'Conditions d\'utilisation validées'))
                ->add('bdc_id_contact', 'integer', array('label' => 'ID dans la BD Contact', 'constraints' => new Assert\NotBlank()))
                ->add('bdc_id_structure', 'integer', array('label' => 'ID de la structure dans la BD Contact', 'constraints' => new Assert\NotBlank()))
                ->add('mail', 'email', array('label' => 'Adresse mail'))
                ->add('telephone', 'text', array('label' => 'Téléphone portable (pour les notifications) sur le format +33600000000'))
                ->add('roles', 'text', array('label' => 'Rôles (ROLE_USER ,ROLE_ADMIN) - ROLE_USER est obligatoire pour tous les utilisateurs'))
                ->add('password', 'password', array('data' => null))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();

            // insert
            if ($username === 'nouveau') {
                $data['date_inscription'] = date('Y-m-d H:i:s');


                $encoder = $app['security.encoder_factory']->getEncoder(currentUser($app));
                $encoded = $encoder->encodePassword($data['password'], null);
                $data['password'] = $encoded;

                $U = new Utilisateur($app['pdo'], null);
                $U->insert($app['pdo'], $data);
            }

            // update
            else {

                $U = new Utilisateur($app['pdo'], $username);
                unset($data['tabFavoris']);

                if ($data['password'] != null) {
                    $encoder = $app['security.encoder_factory']->getEncoder(currentUser($app));
                    /*
                     * https://symfony.com/doc/current/reference/configuration/security.html#using-the-bcrypt-password-encoder
                     * A salt for each new password is generated automatically and need not be persisted. 
                     * Since an encoded password contains the salt used to encode it, persisting the encoded password alone is enough.
                     */
                    $encoded = $encoder->encodePassword($data['password'], '');
                    $data['password'] = $encoded;
                } else {
                    unset($data['password']);
                }

                $U->update($app['pdo'], $data);
            }
            return $app->redirect($app['url_generator']->generate('utilisateurs'));
        }

        return $app['twig']->render('utilisateur/utilisateur.twig', ['U' => $U, 'form' => $form->createView()]);
    } else {
        $app->abort(403);
    }
})->bind('utilisateur');





$app->match('/utilisateur/{username}', function(Request $request, $username) use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_USER') and $app['session']->get('U')->cond_util_validees and $app['session']->get('U')->username === $username) {

        $m = null;

        $Util = new Utilisateur($app['pdo'], $username);
        $U = get_object_vars($Util);


        $form = $app['form.factory']->createBuilder('form', $U)
                ->add('prenom', 'text', array('constraints' => new Assert\NotBlank()))
                ->add('nom', 'text', array('constraints' => new Assert\NotBlank()))
                ->add('mail', 'email', array('label' => 'Adresse mail'))
                ->add('telephone', 'text', array('label' => 'Téléphone portable (pour les notifications) sur le format +33600000000'))
                ->add('password', 'password', array('label' => 'Mot de passe (laisser vide pour conserver l\'actuel)', 'data' => null))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {


            $data = $form->getData();


            $U = new Utilisateur($app['pdo'], $username);
            unset($data['tabFavoris']);

            if ($data['password'] != null) {
                $encoder = $app['security.encoder_factory']->getEncoder(currentUser($app));
                $encoded = $encoder->encodePassword($data['password'], null);
                $data['password'] = $encoded;
            } else {
                unset($data['password']);
            }

            $up = $U->update($app['pdo'], $data);
            $U = new Utilisateur($app['pdo'], $username);
            $app['session']->set('U', $U);

            if (json_decode($up)->status === 'success') {
                $m = '<div class="alert alert-success alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Informations modifiées.</div>';
            } else {
                $m = '<div class="alert alert-danger alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Echec lors de la mise à jour.</div>';
            }


            $app['session']->getFlashBag()->add('message', $m);
        }

        return $app['twig']->render('utilisateur/utilisateur.twig', ['U' => $U, 'form' => $form->createView()]);
    } else {
        $app->abort(403);
    }
})->bind('utilisateur-compte');





$app->match('/admin/utilisateur/{username}/droits', function(Request $request, $username) use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_ADMIN') and $app['session']->get('U')->cond_util_validees) {
        $Util = new Utilisateur($app['pdo'], $username);
        $droits = $Util->chargeDroits($app['pdo']);


        $form = $app['form.factory']->createBuilder('form', $droits)
                ->add('ROLE_LECTEUR', 'text', array('label' => 'Lecteur', 'constraints' => new Assert\NotBlank()))
                ->add('ROLE_CONTRIB', 'text', array('label' => 'Contributeur', 'constraints' => new Assert\NotBlank()))
                ->add('ROLE_GESTIONNAIRE', 'text', array('label' => 'Gestionnaire', 'constraints' => new Assert\NotBlank()))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $Util->metAJourDroits($app['pdo'], $data);
        }


        $tabP = chargeListePointsSuivi($app['pdo'], $app['session']->get('U'));

        return $app['twig']->render('utilisateur/utilisateur-droits.twig', ['U' => $Util, 'form' => $form->createView(), 'tabP' => $tabP]);
    } else {
        $app->abort(403);
    }
})->bind('utilisateur-droits');
