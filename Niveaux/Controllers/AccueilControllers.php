<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

$app->get('/', function() use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_USER') and ( isset($app['session']->get('U')->cond_util_validees) and $app['session']->get('U')->cond_util_validees)) {
        return $app->redirect($app['url_generator']->generate('points-suivi'));
    } else {
        return $app['twig']->render('accueil/accueil.twig');
    }
})->bind('accueil');




$app->get('/login', function(Request $request) use ($app) {

    @session_destroy();
    return $app['twig']->render('accueil/login.twig', array(
                'error' => $app['security.last_error']($request),
                'last_username' => $app['session']->get('_security.last_username'),
    ));
})->bind('login');



$app->match('/control', function() use($app) {

    $token = $app['security.token_storage']->getToken();

    if (null !== $token) {
        $user = $token->getUser();
        $U = new Utilisateur($app['pdo'], $user->getUsername());
        $app['session']->set('U', $U);
        $U->traceConnexion($app['pdo']);

    }

    if (!$U->cond_util_validees) {
        $to = $app['url_generator']->generate('conditions-utilisation');
    } elseif ($app['session']->get('_security.admin.target_path') != null) {
        $to = $app['session']->get('_security.admin.target_path');
    } else {
        $to = $app['url_generator']->generate('points-suivi');
    }
    return $app->redirect($to);
});

$app->error(function (\Exception $e, $code) {
    $message = null;

    switch ($code) {
        case 404:
            return;
            break;
        case 403:
            $message = 'Accès limité';
            break;
        default:
            return;
    }
    return new Response($message);
});
