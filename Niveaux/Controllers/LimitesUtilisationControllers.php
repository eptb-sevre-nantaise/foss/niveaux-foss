<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

$app->match('/conditions-utilisation', function() use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_USER')) {
        return $app['twig']->render('cgu/conditions-utilisation.twig', ['conditionsutilisation' => $app['CONDITIONSCOMPLETES']]);
    } else {
        $app->abort(403);
    }
})->bind('conditions-utilisation');

$app->match('/conditions-utilisation/validation', function() use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_USER')) {

        $token = $app['security.token_storage']->getToken();
        $user = $token->getUser();
        $U = new Utilisateur($app['pdo'], $user->getUsername());
        //var_dump($U);

        $now = new \DateTime('now', new DateTimeZone($app['TIMEZONE']));
        if ($U->updatecol('cond_util_validees', true, $app['pdo']) and
                $U->updatecol('date_valid_cond_util', $now->format('Y-m-d H:i:s'), $app['pdo'])) {

            envoieMail($app['MAILADMIN'], $app['MAILEXP'], $app['NAMEEXP'], 'Validation des conditions d\'utilisation par ' . $U->prenom . ' ' . $U->nom, $app['BRAND']);
            if ($app['MAILADMIN2'] != null) {
                envoieMail($app['MAILADMIN2'], $app['MAILEXP'], $app['NAMEEXP'], 'Validation des conditions d\'utilisation par ' . $U->prenom . ' ' . $U->nom, $app['BRAND']);
            }
            $app['session']->set('U', new Utilisateur($app['pdo'], $U->username));
            // var_dump($app['session']->get('U'));

            return $app->redirect($app['url_generator']->generate('points-suivi'));
        } else {
            return 'Erreur lors de la validation';
        }
    } else {
        $app->abort(403);
    }
})->bind('conditions-utilisation-validation');



$app->match('/conditions-utilisation/non-validation', function() use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_USER')) {
        $U = $app['session']->get('U');
        envoieMail($app['MAILADMIN'], $app['MAILEXP'], $app['NAMEEXP'], 'NON validation des conditions d\'utilisation par ' . $U->prenom . ' ' . $U->nom, BRAND);
        return $app->redirect($app['url_generator']->generate('logout'));
    } else {
        $app->abort(403);
    }
})->bind('conditions-utilisation-non-validation');


$app->post('/conditions-utilisation/bandeau', function(Request $request) use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_USER')) {

        if ($request->get('action') === 'off') {

            $app['session']->set('bandeau', 'off');
            return '';
        }
    } else {
        $app->abort(403);
    }
})->bind('conditions-utilisation-bandeau');
