<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

$app->match('/admin/parametres', function(Request $request) use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_ADMIN') and $app['session']->get('U')->cond_util_validees) {

        $mess = null;
        $parametres = chargeParametresAppli($app['pdo']);

        // tableau des valeurs
        foreach ($parametres as $p) {
            $valeurs[$p['alias']] = $p['valeur'];
        }

        $formbuilder = $app['form.factory']->createBuilder('form', $valeurs);

        foreach ($parametres as $p) {

            $formbuilder->add($p['alias'], $p['type'], array('label' => $p['alias'] . ' (' . $p['parametre'] . ')', 'constraints' => new Assert\NotBlank()));
        }

        $form = $formbuilder->getForm();


        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();


            foreach ($parametres as $p) {

                updatecol('parametres', 'alias', $p['alias'], 'valeur', $data[$p['alias']], $app['pdo']);
            }

            $mess = '<div class="alert alert-success alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Paramètres modifiés.</div>';
            $app['session']->getFlashBag()->add('message', $mess);
        }

        return $app['twig']->render('admin/admin-parametres.twig', ['form' => $form->createView()]);
    } else {
        $app->abort(403);
    }
})->bind('admin-parametres');



$app->match('/admin/test-sms', function(Request $request) use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_ADMIN') and $app['session']->get('U')->cond_util_validees) {
            testSMS();
        }
}) ;