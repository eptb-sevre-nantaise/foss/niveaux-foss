<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

$app->match('/carte/{id}', function($id) use($app) {

    $P = new PointSuivi($app['pdo'], $id);
    if ($app['session']->get('U')->aAcces('ROLE_LECTEUR', $P->id_point_suivi, $app['pdo']) and $app['session']->get('U')->cond_util_validees) {

        $coords = explode(' ', substr($P->geom_text4326, 6, strlen($P->geom_text4326) - 7));
        return $app['twig']->render('carte/carte-simple.twig', ['lat' => $coords[1], 'lng' => $coords[0]]);
    } else {
        $app->abort(403);
    }
})->bind('carte-simple');

