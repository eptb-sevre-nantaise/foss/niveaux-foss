<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

$app->match('/admin/notifications', function() use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_ADMIN') and $app['session']->get('U')->cond_util_validees) {


        return $app['twig']->render('admin/admin-notifications.twig', ['notifications' => chargeNotifications($app['pdo']), 'envois' => chargeEnvoisNotifications($app)]);
    } else {
        $app->abort(403);
    }
})->bind('admin-notifications');



$app->match('/{username}/notifications', function($username) use($app) {

    if (($app['security.authorization_checker']->isGranted('ROLE_USER') and $username === $app['session']->get('U')->username) and $app['session']->get('U')->cond_util_validees) {

        return $app['twig']->render('notification/notifications.twig', ['notifications' => $app['session']->get('U')->chargeNotifications($app['pdo'])]);
    } else {
        $app->abort(403);
    }
})->bind('notifications');



$app->match('/{username}/notification/test-envoi-mail', function($username) use($app) {


    if ($app['security.authorization_checker']->isGranted('ROLE_USER') and $app['session']->get('U')->cond_util_validees
    ) {

        if (
                envoieMail($app['session']->get('U')->mail, MAILEXP, NAMEEXP, 'Message test issu de ' . $app['BRAND'] . ' (' . $app['BRANDSTRUCTURE'] . ')', 'Notifiation Test')) {
            $m = '<div class="alert alert-success alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Une notification test vous a été envoyée.</div>';
        } else {
            $m = '<div class="alert alert-danger alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Echec lors de l\'envoi de la notification.</div>';
        }
        $app['session']->getFlashBag()->add('message', $m);
        return $app->redirect($app['url_generator']->generate('notifications', ['username' => $app['session']->get('U')->username]));
    } else {
        $app->abort(403);
    }
})->bind('notification-test-envoi-mail');


$app->match('/{username}/notification/{id_notification}', function(Request $request, $username, $id_notification) use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_USER') and $app['session']->get('U')->cond_util_validees
    ) {

        $n = $N = null;
        if ($id_notification !== 'nouvelle') {
            $N = new Notification($app['pdo'], $id_notification);
            $n = get_object_vars($N);
        }

        // nouvelle notification
        // ou modification pour le créateur ou l'administrateur
        if ($id_notification === 'nouvelle' or ( $N->id_user == $app['session']->get('U')->id_user) or $app['security.authorization_checker']->isGranted('ROLE_ADMIN')) {

            $choices = array();
            if ($app['session']->get('U')->mail != null)
                $choices['mail'] = 'Mail';
            /*
             * spécifique à la démo, le type SMS est désactivé
             */
            /*
              if ($app['session']->get('U')->telephone != null)
              $choices['sms'] = 'SMS';
             */

            $form = $app['form.factory']->createBuilder('form', $n)
                    ->add('id_point_suivi', 'choice', array('choices' => chargeListePointsSuivi($app['pdo'], $app['session']->get('U')), 'label' => 'Station de suivi', 'constraints' => new Assert\NotBlank()))
                    ->add('seuil', 'number', array('label' => 'Seuil de déclenchement', 'constraints' => new Assert\NotBlank()))
                    ->add('fonction_notification', 'choice', array('choices' => array('valeurActuelleBrute' => 'Normal (dernière valeur brute)', 'moyenneSurDuree3Jours' => 'Moyenne sur 3 jours'), 'label' => 'Fonction de notification', 'constraints' => new Assert\NotBlank()))
                    ->add('montant_descendant', 'choice', array('choices' => array('montant' => 'Montant', 'descendant' => 'Descendant'), 'label' => 'Déclenchement de la notification', 'constraints' => new Assert\NotBlank()))
                    ->add('type_notification', 'choice', array('choices' => $choices, 'label' => 'Type de notification', 'constraints' => new Assert\NotBlank()))
                    ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {



                $data = $form->getData();


                if ($data['fonction_notification'] == 'valeurActuelleBrute') {
                    $data['fonction'] = 'valeurActuelleBrute';
                    $data['fonction_parametres'] = null;
                }
                if ($data['fonction_notification'] == 'moyenneSurDuree3Jours') {
                    $data['fonction'] = 'moyenneSurDuree';
                    $data['fonction_parametres'] = '{"duree":"-3 days"}';
                }

                // var_dump($data);
                // die();
                unset($data['fonction_notification']);


                // insert
                if ($id_notification === 'nouvelle') {

                    $data['id_user'] = $app['session']->get('U')->id_user;
                    if ($app['session']->get('U')->aAcces('ROLE_LECTEUR', $data['id_point_suivi'], $app['pdo'])) {
                        $N = new Notification($app['pdo'], null);
                        $N->insert($app['pdo'], $data);
                    } else {
                        $app->abort(403);
                    }
                }

                // update
                else {

                    $N = new Notification($app['pdo'], $id_notification);
                    if (
                            (
                            $app['session']->get('U')->aAcces('ROLE_LECTEUR', $data['id_point_suivi'], $app['pdo'])
                            and ( $N->id_user == $app['session']->get('U')->id_user)
                            )
                            or
                            $app['security.authorization_checker']->isGranted('ROLE_ADMIN')
                    ) {
                        $N->update($app['pdo'], array('id_notification' => $id_notification, 'seuil' => $data['seuil'], 'type_notification' => $data['type_notification'], 'montant_descendant' => $data['montant_descendant'], 'id_point_suivi' => $data['id_point_suivi'], 'date_modification' => date('Y-m-d H:i:s'), 'fonction' => $data['fonction'], 'fonction_parametres' => $data['fonction_parametres']));
                    } else {
                        $app->abort(403);
                    }
                }


                return $app->redirect($app['url_generator']->generate('notifications', ['username' => $app['session']->get('U')->username]));
            }
            return $app['twig']->render('notification/notification.twig', ['N' => $N, 'form' => $form->createView()]);
        } else {
            $app->abort(403);
        }
    } else {
        $app->abort(403);
    }
})->bind('notification');



$app->match('/{username}/notification/suppression/{id_notification}', function(Request $request, $username, $id_notification) use($app) {

    if (
            (($app['security.authorization_checker']->isGranted('ROLE_USER') and $app['session']->get('U')->username === $username)
            or $app['security.authorization_checker']->isGranted('ROLE_ADMIN')) and $app['session']->get('U')->cond_util_validees) {
        $N = new Notification($app['pdo'], $id_notification);

        if (($N->id_user == $app['session']->get('U')->id_user) or $app['security.authorization_checker']->isGranted('ROLE_ADMIN')) {

            $N->desactive($app['pdo']);

            if ($app['security.authorization_checker']->isGranted('ROLE_ADMIN')) {
                return $app->redirect($app['url_generator']->generate('admin-notifications'));
            } else {
                return $app->redirect($app['url_generator']->generate('notifications', ['username' => $app['session']->get('U')->username]));
            }
        } else {
            $app->abort(403);
        }
    } else {
        $app->abort(403);
    }
})->bind('notification-suppression');



$app->match('/{username}/notification/{id_notification}/envois', function(Request $request, $username, $id_notification) use($app) {

    if (
            (($app['security.authorization_checker']->isGranted('ROLE_USER') and $app['session']->get('U')->username === $username)
            or $app['security.authorization_checker']->isGranted('ROLE_ADMIN')) and $app['session']->get('U')->cond_util_validees) {
        $N = new Notification($app['pdo'], $id_notification);

        if ($N->id_user == $app['session']->get('U')->id_user or $app['security.authorization_checker']->isGranted('ROLE_ADMIN')) {
            return $app['twig']->render('notification/notification-envois.twig', ['tabE' => $N->chargeEnvois($app['pdo']), 'username' => $username]);
        } else {
            $app->abort(403);
        }
    } else {
        $app->abort(403);
    }
})->bind('notification-envois');
