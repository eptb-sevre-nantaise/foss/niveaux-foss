<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

/*
 * Accessible sans être connecté pour être interrogé par exemple par Osiris inondations
 */

$app->match('/csv/{alias}/{depuis}', function($alias, $depuis, Request $request) use($app) {

    $P = new PointSuivi($app['pdo'], null, $alias);
    $tabV = $P->chargeValeurs($app['pdo'], date_sub(date_create('now'), date_interval_create_from_date_string($depuis)));
    return $app['twig']->render('csv/csv.twig', ['P' => $P, 'tabV' => $tabV]);
})->bind('csv');


$app->match('/csv/releve-{type}/{alias}/{depuis}', function($type, $alias, $depuis) use($app) {

    $P = new PointSuivi($app['pdo'], null, $alias);
    if ($app['session']->get('U')->aAcces('ROLE_LECTEUR', $P->id_point_suivi, $app['pdo']) and $app['session']->get('U')->cond_util_validees) {
        $tabV = $P->chargeValeurs($app['pdo'], date_sub(date_create('now'), date_interval_create_from_date_string($depuis)), null, $type);
        return $app['twig']->render('csv/csv-type.twig', ['P' => $P, 'tabV' => $tabV]);
    } else {
        $app->abort(403);
    }
})->bind('csv-type');
