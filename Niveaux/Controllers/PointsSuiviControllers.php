<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

$app->match('/points-suivi', function() use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_USER') and $app['session']->get('U')->cond_util_validees) {

        return $app['twig']->render('point-suivi/points-suivi.twig', ['points' => chargePointsSuivi($app['session']->get('U')->id_user, $app['pdo']), 'tabLayers' => chargeLayers($app['LAYERS'])]);
    } else {

        return $app->redirect($app['url_generator']->generate('logout'));
    }
})->bind('points-suivi');


$app->match('/points-suivi-liste', function() use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_USER') and $app['session']->get('U')->cond_util_validees) {

        return $app['twig']->render('point-suivi/points-suivi-liste.twig', ['points' => chargePointsSuivi($app['session']->get('U')->id_user, $app['pdo'])]);
    } else {

        return $app->redirect($app['url_generator']->generate('logout'));
    }
})->bind('points-suivi-liste');




$app->match('/point-suivi/{alias}/favori/{action}', function($alias, $action) use($app) {


    if ($app['security.authorization_checker']->isGranted('ROLE_USER') and $app['session']->get('U')->cond_util_validees) {

        $P = new PointSuivi($app['pdo'], null, $alias);

        if ($app['session']->get('U')->aAcces('ROLE_LECTEUR', $P->id_point_suivi, $app['pdo'])) {

            if ($action === 'ajout') {
                $app['session']->get('U')->ajoutePointSuiviFavori($P->id_point_suivi, $app['pdo']);

                return $app->redirect($app['url_generator']->generate('points-suivi-liste'));
            }
            if ($action === 'suppression') {
                $app['session']->get('U')->supprimePointSuiviFavori($P->id_point_suivi, $app['pdo']);
                return $app->redirect($app['url_generator']->generate('points-suivi-liste'));
            }
        }
    }
})->bind('point-suivi-favori');



// CUMULS


$app->match('/cumul/{alias}/{debut}/{fin}', function($alias, $debut, $fin) use($app) {

    $P = new PointSuivi($app['pdo'], null, $alias);

    if ($app['security.authorization_checker']->isGranted('ROLE_USER') and $app['session']->get('U')->aAcces('ROLE_LECTEUR', $P->id_point_suivi, $app['pdo'])) {

        return ' ' . calculeCumul($P, $debut, $fin, $app['pdo']);
    } else {
        $app->abort(403);
    }
})->bind('cumul');



$app->match('/{type}/point-suivi/{alias}', function(Request $request, $type, $alias) use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_USER') and $app['session']->get('U')->cond_util_validees) {
        $P = new PointSuivi($app['pdo'], null, $alias);

        if ($app['session']->get('U')->aAcces('ROLE_LECTEUR', $P->id_point_suivi, $app['pdo'])) {

            if ($request->query->get('depuis') == null) {
                if ($app['session']->get('depuis') != null) {
                    $depuis = $app['session']->get('depuis');
                } else {
                    $depuis = $app['DEFAULTDURATION'];
                    $app['session']->set('depuis', $depuis);
                }
            } else {

                $depuis = htmlentities(str_replace(array('jours', 'jour'), 'days', str_replace(array('heures', 'heure'), 'hours', $request->query->get('depuis'))), ENT_QUOTES);
                $app['session']->set('depuis', $depuis);
            }



            if ($request->query->get('heuresslmt') == null) {
                if ($app['session']->get('heuresslmt') != null) {
                    $heuresslmt = $app['session']->get('heuresslmt');
                } else {
                    $heuresslmt = '1';
                    $app['session']->set('heuresslmt', $heuresslmt);
                }
            } else {

                $heuresslmt = htmlentities($request->query->get('heuresslmt'), ENT_QUOTES);               
                $app['session']->set('heuresslmt', $heuresslmt);
            }

         

            if ($request->query->get('cumul') == null) {
                if ($app['session']->get('cumuldepuis') != null) {
                    $cumul = $app['session']->get('cumul');
                } else {
                    $cumul = $app['DEFAULTDURATIONTOTALRAIN'];
                    $app['session']->set('cumul', $cumul);
                }
            } else {
                $cumul = $request->query->get('cumul');
                $app['session']->set('cumul', $request->query->get('cumul'));
            }


            $tabV = $P->chargeValeurs($app['pdo'], date_sub(date_create('now'), date_interval_create_from_date_string($depuis)), null, null, null, $heuresslmt);
            $max = -99999;
            foreach ($tabV as $V) {

                if (($V['valeur'] + 1.5) > $max)
                    $max = $V['valeur'] + 1.5;
            }

            $P->chargeSeuils($app['pdo'], $max);

            return $app['twig']->render('point-suivi/point-suivi.twig', ['P' => $P, 'tabV' => $tabV, 'tabS' => $P->tabSeuils, 'depuis' => $depuis, 'cumul' => $cumul, 'heuresslmt' => $heuresslmt]);
        } else {
            $app->abort(403);
        }
    } else {

        return $app->redirect($app['url_generator']->generate('logout'));
    }
})->bind('point-suivi');






$app->match('/point-suivi/{alias}', function(Request $request, $alias) use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_USER') and $app['session']->get('U')->cond_util_validees) {
        $P = new PointSuivi($app['pdo'], null, $alias);

        if ($app['session']->get('U')->aAcces('ROLE_LECTEUR', $P->id_point_suivi, $app['pdo'])) {

            $P->chargeSeuils($app['pdo']);
            return $app['twig']->render('point-suivi/point-suivi-info.twig', ['P' => $P]);
        } else {
            $app->abort(403);
        }
    } else {

        return $app->redirect($app['url_generator']->generate('logout'));
    }
})->bind('point-suivi-info');




$app->match('/point-suivi/form/{alias}', function(Request $request, $alias) use($app) {


    $p = $P = null;
    if ($alias !== 'nouveau') {
        $P = new PointSuivi($app['pdo'], null, $alias);
        $p = get_object_vars($P);
        $geom = $P->geom_text;
        $id = $P->id_point_suivi;
    } else {
        $geom = 'POINT(0 0)';
        $id = 0;
    }

    if ($app['session']->get('U')->aAcces('ROLE_GESTIONNAIRE', $id, $app['pdo']) and $app['session']->get('U')->cond_util_validees) {


        $form = $app['form.factory']->createBuilder('form', $p)
                ->add('point_suivi', 'text', array('label' => 'Nom du point de suivi', 'constraints' => new Assert\NotBlank()))
                ->add('alias', 'text', array('label' => 'Alias (nom sans espace, accents, ni caractères spéciaux)', 'constraints' => new Assert\NotBlank()))
                ->add('code_station', 'text', array('label' => 'Code du point de suivi (code DREAL, MétéoFrance si existant)'))
                ->add('nom_station_officiel', 'text', array('label' => 'Nom officiel (dans la vue tableau sur vigicrue)'))
                ->add('type_suivi', 'choice', array('choices' => array('valeur' => 'Valeur (numérique)', 'seuil' => 'Seuil'), 'label' => 'Type de suivi', 'constraints' => new Assert\NotBlank()))
                ->add('type_point', 'choice', array('choices' => array('hauteur' => 'Hauteur d\'eau', 'debit' => 'Débit', 'pluvio' => 'Pluviométrie', 'seuil' => 'Seuil'), 'label' => 'Type de suivi', 'constraints' => new Assert\NotBlank()))
                ->add('type_releve', 'choice', array('choices' => array('manuel' => 'Manuel', 'auto' => 'Automatique (télétransmis)'), 'label' => 'Type de relevé (le plus souvent)', 'constraints' => new Assert\NotBlank()))
                ->add('structure_gestionnaire', 'text', array('label' => 'Structure gestionnaire du point de suivi', 'constraints' => new Assert\NotBlank()))
                ->add('bdc_id_structure_gestionnaire', 'integer', array('label' => 'ID de la structure gestionnaire dans la BD Contacts', 'constraints' => new Assert\NotBlank()))
                ->add('lien_vigicrue', 'text', array('label' => 'Lien vers Vigicrue'))
                ->add('lien_banquehydro', 'text', array('label' => 'Lien vers la Banque Hydro'))
                ->add('lien_infoclimat', 'text', array('label' => 'Lien vers Infoclimat'))
                ->add('lien_autre', 'text', array('label' => 'Lien vers une autre source'))
                ->add('carthage_cours_eau', 'text', array('label' => 'Nom du cours d\'eau (BD Carthage) (hors pluvio)'))
                ->add('carthage_id_cours_eau', 'text', array('label' => 'ID du cours d\'eau (BD Carthage) (hors pluvio)'))
                ->add('departement', 'integer', array('label' => 'Département (en chiffres)'))
                ->add('insee_commune', 'integer', array('label' => 'Code INSEE commune'))
                ->add('localisation', 'text', array('label' => 'Localisation', 'data' => $geom, 'constraints' => new Assert\NotBlank()))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();

            if ($alias === 'nouveau') {

                $P = new PointSuivi($app['pdo'], null);

                // conversion de la géométrie             
                $data['geom'] = convertGeomFromText($app['pdo'], $data['localisation']);
                unset($data['localisation']);
                unset($data['tabSeuils']);
                $P->insert($app['pdo'], $data);
            } else {

                // conversion de la géométrie             
                $data['geom'] = convertGeomFromText($app['pdo'], $data['localisation']);

                unset($data['unite']);
                unset($data['element_mesure']);
                unset($data['geom_text']);
                unset($data['geom_text4326']);
                unset($data['localisation']);
                unset($data['tabSeuils']);

                $P->update($app['pdo'], $data);
            }


            return $app->redirect($app['url_generator']->generate('points-suivi'));
        }
    } else {
        $app->abort(403);
    }

    return $app['twig']->render('point-suivi/point-suivi-form.twig', ['P' => $P, 'form' => $form->createView()]);
})->bind('point-suivi-form');




// SEUIL

$app->match('/point-suivi/{alias}/seuil/{id_seuil}/form', function(Request $request, $id_seuil, $alias) use($app) {


    $P = new PointSuivi($app['pdo'], null, $alias);



    if ($app['session']->get('U')->aAcces('ROLE_GESTIONNAIRE', $P->id_point_suivi, $app['pdo']) and $app['session']->get('U')->cond_util_validees) {

        if ($id_seuil === 'nouveau') {
            $s = null;
        } else {
            $S = new Seuil($app['pdo'], $id_seuil);
            $s = get_object_vars($S);
        }

        $form = $app['form.factory']->createBuilder('form', $s)
                ->add('nom', 'text', array('label' => 'Intitulé du seuil (ex : Février 2001, Q10...)', 'constraints' => new Assert\NotBlank()))
                ->add('valeur', 'number', array('label' => 'Niveau du seuil', 'constraints' => new Assert\NotBlank()))
                ->add('date_seuil', 'text', array('required' => false, 'label' => 'Date où le seuil a été atteint (optionnel) (format : jj/mm/aaaa hh:mm)'))
                ->add('commentaire', 'textarea', array('label' => 'Commentaire'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $data['id_point_suivi'] = $P->id_point_suivi;


            if ($id_seuil === 'nouveau') {

                $S = new Seuil($app['pdo'], null);
                $S->insert($app['pdo'], $data);
            } else {

                $S->update($app['pdo'], $data);
            }


            $P = new PointSuivi($app['pdo'], $data['id_point_suivi']);
            return $app->redirect($app['url_generator']->generate('point-suivi-info', ['alias' => $P->alias]));
        }

        return $app['twig']->render('point-suivi/seuil-form.twig', ['P' => $P, 'S' => $s, 'form' => $form->createView()]);
    } else {
        $app->abort(403);
    }
})->bind('seuil-form');



$app->match('/point-suivi/{alias}/seuil/{id_seuil}/suppression', function(Request $request, $id_seuil, $alias) use($app) {


    $P = new PointSuivi($app['pdo'], null, $alias);
    if ($app['session']->get('U')->aAcces('ROLE_GESTIONNAIRE', $P->id_point_suivi, $app['pdo']) and $app['session']->get('U')->cond_util_validees) {

        $S = new Seuil($app['pdo'], $id_seuil);

        if ($S->delete($app['pdo'])) {
            return $app->redirect($app['url_generator']->generate('point-suivi/point-suivi-info', ['alias' => $P->alias]));
        } else {
            $app->abort(403);
        }
    } else {
        $app->abort(403);
    }
})->bind('seuil-suppression');
