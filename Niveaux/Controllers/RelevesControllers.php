<?php


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;


$app->match('/releve/{alias}/nouveau', function(Request $request, $alias) use($app) {


    $P = new PointSuivi($app['pdo'], null, $alias);

    if (isset($P->id_point_suivi) and $app['session']->get('U')->aAcces('ROLE_CONTRIB', $P->id_point_suivi, $app['pdo']) and $app['session']->get('U')->cond_util_validees) {



        $form = $app['form.factory']->createBuilder('form')
                ->add('valeur', 'number', array('label' => 'Valeur observée', 'constraints' => new Assert\NotBlank()))
                ->add('date_valeur', 'date', array('label' => 'Date de l\'observation (jj/mm/aaaa)', 'data' => new \DateTime('now'), 'attr' => array('class' => 'date'), 'format' => 'dd/MM/yyyy', 'constraints' => new Assert\NotBlank()))
                ->add('heure_valeur', 'time', array('label' => 'Heure de l\'observation (hh:mm)', 'data' => new \DateTime('now'), 'attr' => array('class' => 'date'), 'widget' => 'choice', 'constraints' => new Assert\NotBlank()))
                ->getForm();


        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();

            $dateR = $data['date_valeur']->format('Y-m-d');
            $heureR = $data['heure_valeur']->format('H:i:s');

            $data['id_point_suivi'] = $P->id_point_suivi;


            $myDate = new DateTime($dateR . ' ' . $heureR, new DateTimeZone($app['TIMEZONE']));
            //var_dump($heureR) ;

            $R = new Releve($app);
            if ($R->insert(
                            array(
                        'id_user' => $app['session']->get('U')->id_user,
                        'date_releve' => date('Y-m-d H:i:s'),
                        'type_releve' => 'manuel',
                        'type_suivi' => 'valeur',
                        'id_tache' => 0), array(
                        array(
                            'id_point_suivi' => $data['id_point_suivi'],
                            'valeurs' => array(0 => array('date_valeur' => $myDate, 'valeur' => $data['valeur']))
                        )
                            ), $app['pdo']
                    )) {

                return $app->redirect($app['url_generator']->generate('point-suivi', ['alias' => $P->alias, 'type' => $P->type_point]));
            } else {

                return $app['twig']->render('erreur.twig', ['erreur' => 'Erreur lors de l\'insertion des données']);
            }
        }


        return $app['twig']->render('releve/releve.twig', ['P' => $P, 'form' => $form->createView()]);
    } else {
        $app->abort(403);
    }
})->bind('releve-nouveau');




$app->match('/admin/releves', function(Request $request) use($app) {

    if ($app['security.authorization_checker']->isGranted('ROLE_ADMIN') and $app['session']->get('U')->cond_util_validees) {

        return $app['twig']->render('releve/releves.twig', ['tabR' => chargeReleves($app['pdo'])]);
    } else {
        $app->abort(403);
    }
})->bind('releves');

