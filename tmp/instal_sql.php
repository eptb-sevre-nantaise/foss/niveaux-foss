<?php
/*
 * Copiez le code SQL ci-dessous et exécutez le dans votre base de données 
 * ATTENTION ce code crée un schema 'niveaux', assurez-vous que votre base n'en comprend pas déjà un
 */

die() ;
?>

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.17
-- Dumped by pg_dump version 9.6.6


SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 10 (class 2615 OID 101747925)
-- Name: niveaux; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA niveaux;


--
-- TOC entry 4103 (class 0 OID 0)
-- Dependencies: 10
-- Name: SCHEMA niveaux; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA niveaux IS 'Outil de suivi et partage des niveaux d''eau';


SET search_path = niveaux, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 324 (class 1259 OID 101747926)
-- Name: utilisateur; Type: TABLE; Schema: niveaux; Owner: -
--

CREATE TABLE utilisateur (
    id_user integer NOT NULL,
    username character varying,
    prenom character varying,
    nom character varying,
    bdc_id_contact integer,
    bdc_id_structure integer,
    password character varying,
    mail character varying,
    telephone character varying,
    roles character varying DEFAULT 'ROLE_USER'::character varying,
    date_inscription character varying DEFAULT now(),
    date_valid_cond_util timestamp with time zone,
    cond_util_validees integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 4104 (class 0 OID 0)
-- Dependencies: 324
-- Name: COLUMN utilisateur.bdc_id_contact; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN utilisateur.bdc_id_contact IS 'Id de l''utilisateur dans la base de données contact de l''EPTB SN';


--
-- TOC entry 4105 (class 0 OID 0)
-- Dependencies: 324
-- Name: COLUMN utilisateur.bdc_id_structure; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN utilisateur.bdc_id_structure IS 'Id de la structure de l''utilisateur dans la base de données contacts de l''EPTB SN';


--
-- TOC entry 325 (class 1259 OID 101747935)
-- Name: utilisateur_connexion; Type: TABLE; Schema: niveaux; Owner: -
--

CREATE TABLE utilisateur_connexion (
    id_user integer,
    date_connexion timestamp with time zone DEFAULT now(),
    id_connexion integer NOT NULL,
    type_connexion character varying DEFAULT 'appli_web'::character varying
);


--
-- TOC entry 353 (class 1259 OID 101748175)
-- Name: bilan_connexions; Type: VIEW; Schema: niveaux; Owner: -
--

CREATE VIEW bilan_connexions AS
 SELECT c.id_user,
    u.username,
    count(*) AS nb_connexion
   FROM (utilisateur_connexion c
     LEFT JOIN utilisateur u ON ((u.id_user = c.id_user)))
  GROUP BY c.id_user, u.username
  ORDER BY (count(*)) DESC;


--
-- TOC entry 326 (class 1259 OID 101747943)
-- Name: notification_envoi; Type: TABLE; Schema: niveaux; Owner: -
--

CREATE TABLE notification_envoi (
    id_envoi_notification integer NOT NULL,
    id_notification integer,
    date_envoi_notification timestamp with time zone DEFAULT now(),
    id_releve integer,
    statut character varying,
    contenu_notification json,
    releve_valeur character varying,
    releve_valeur_date timestamp with time zone
);


--
-- TOC entry 4106 (class 0 OID 0)
-- Dependencies: 326
-- Name: COLUMN notification_envoi.id_releve; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN notification_envoi.id_releve IS 'relevé ayant déclenché la notification';


--
-- TOC entry 4107 (class 0 OID 0)
-- Dependencies: 326
-- Name: COLUMN notification_envoi.statut; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN notification_envoi.statut IS 'succes, echec';


--
-- TOC entry 4108 (class 0 OID 0)
-- Dependencies: 326
-- Name: COLUMN notification_envoi.releve_valeur; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN notification_envoi.releve_valeur IS 'valeur ayant déclenché la notification';


--
-- TOC entry 4109 (class 0 OID 0)
-- Dependencies: 326
-- Name: COLUMN notification_envoi.releve_valeur_date; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN notification_envoi.releve_valeur_date IS 'date de la valeur ayant déclenché la notification';


--
-- TOC entry 327 (class 1259 OID 101747950)
-- Name: envoi_notification_id_envoi_notification_seq; Type: SEQUENCE; Schema: niveaux; Owner: -
--

CREATE SEQUENCE envoi_notification_id_envoi_notification_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4110 (class 0 OID 0)
-- Dependencies: 327
-- Name: envoi_notification_id_envoi_notification_seq; Type: SEQUENCE OWNED BY; Schema: niveaux; Owner: -
--

ALTER SEQUENCE envoi_notification_id_envoi_notification_seq OWNED BY notification_envoi.id_envoi_notification;


--
-- TOC entry 328 (class 1259 OID 101747952)
-- Name: l_user_point_suivi; Type: TABLE; Schema: niveaux; Owner: -
--

CREATE TABLE l_user_point_suivi (
    id_l_user_point_suivi integer NOT NULL,
    id_user integer,
    droit character varying,
    ids_points_suivi integer[]
);


--
-- TOC entry 329 (class 1259 OID 101747958)
-- Name: l_user_point_suivi_favori; Type: TABLE; Schema: niveaux; Owner: -
--

CREATE TABLE l_user_point_suivi_favori (
    id_l_user_pt_suivi_favori integer NOT NULL,
    id_user integer,
    id_point_suivi integer
);


--
-- TOC entry 330 (class 1259 OID 101747961)
-- Name: l_user_point_suivi_favori_id_l_user_pt_suivi_favori_seq; Type: SEQUENCE; Schema: niveaux; Owner: -
--

CREATE SEQUENCE l_user_point_suivi_favori_id_l_user_pt_suivi_favori_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4111 (class 0 OID 0)
-- Dependencies: 330
-- Name: l_user_point_suivi_favori_id_l_user_pt_suivi_favori_seq; Type: SEQUENCE OWNED BY; Schema: niveaux; Owner: -
--

ALTER SEQUENCE l_user_point_suivi_favori_id_l_user_pt_suivi_favori_seq OWNED BY l_user_point_suivi_favori.id_l_user_pt_suivi_favori;


--
-- TOC entry 331 (class 1259 OID 101747963)
-- Name: l_user_point_suivi_id_l_user_point_suivi_seq; Type: SEQUENCE; Schema: niveaux; Owner: -
--

CREATE SEQUENCE l_user_point_suivi_id_l_user_point_suivi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4112 (class 0 OID 0)
-- Dependencies: 331
-- Name: l_user_point_suivi_id_l_user_point_suivi_seq; Type: SEQUENCE OWNED BY; Schema: niveaux; Owner: -
--

ALTER SEQUENCE l_user_point_suivi_id_l_user_point_suivi_seq OWNED BY l_user_point_suivi.id_l_user_point_suivi;


--
-- TOC entry 332 (class 1259 OID 101747965)
-- Name: notification; Type: TABLE; Schema: niveaux; Owner: -
--

CREATE TABLE notification (
    id_notification integer NOT NULL,
    id_point_suivi integer,
    id_user integer,
    type_notification character varying,
    montant_descendant character varying,
    date_creation timestamp with time zone DEFAULT now(),
    date_modification timestamp with time zone DEFAULT now(),
    seuil numeric,
    active integer DEFAULT 1,
    fonction character varying,
    fonction_parametres json
);


--
-- TOC entry 4113 (class 0 OID 0)
-- Dependencies: 332
-- Name: COLUMN notification.type_notification; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN notification.type_notification IS 'sms,mail';


--
-- TOC entry 333 (class 1259 OID 101747974)
-- Name: point_suivi; Type: TABLE; Schema: niveaux; Owner: -
--

CREATE TABLE point_suivi (
    id_point_suivi integer NOT NULL,
    point_suivi character varying,
    type_suivi character varying,
    type_point character varying,
    type_releve character varying,
    structure_gestionnaire character varying,
    bdc_id_structure_gestionnaire integer,
    carthage_cours_eau character varying,
    carthage_id_cours_eau character varying,
    departement character varying,
    insee_commune character varying,
    code_station character varying,
    alias character varying,
    geom public.geometry(Point,2154),
    nom_station_officiel character varying,
    lien_vigicrue character varying,
    lien_banquehydro character varying,
    lien_infoclimat character varying,
    lien_autre character varying
);


--
-- TOC entry 4114 (class 0 OID 0)
-- Dependencies: 333
-- Name: TABLE point_suivi; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON TABLE point_suivi IS 'Station hydrométrique, pluviométrique ou point de repère';


--
-- TOC entry 4115 (class 0 OID 0)
-- Dependencies: 333
-- Name: COLUMN point_suivi.type_suivi; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN point_suivi.type_suivi IS 'type de suivi principal sur cette station : valeur,seuil';


--
-- TOC entry 4116 (class 0 OID 0)
-- Dependencies: 333
-- Name: COLUMN point_suivi.type_point; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN point_suivi.type_point IS 'hauteur,pluvio,seuil';


--
-- TOC entry 4117 (class 0 OID 0)
-- Dependencies: 333
-- Name: COLUMN point_suivi.type_releve; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN point_suivi.type_releve IS 'principal type de relevé (mais pas forcément unique) : auto, manuel';


--
-- TOC entry 4118 (class 0 OID 0)
-- Dependencies: 333
-- Name: COLUMN point_suivi.carthage_cours_eau; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN point_suivi.carthage_cours_eau IS 'nom du cours d''eau dans carthage';


--
-- TOC entry 4119 (class 0 OID 0)
-- Dependencies: 333
-- Name: COLUMN point_suivi.code_station; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN point_suivi.code_station IS 'code banque hydro ou météo france';


--
-- TOC entry 4120 (class 0 OID 0)
-- Dependencies: 333
-- Name: COLUMN point_suivi.geom; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN point_suivi.geom IS 'localisation';


--
-- TOC entry 4121 (class 0 OID 0)
-- Dependencies: 333
-- Name: COLUMN point_suivi.nom_station_officiel; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN point_suivi.nom_station_officiel IS 'nom vigicrue ou autre';


--
-- TOC entry 350 (class 1259 OID 101748151)
-- Name: point_suivi_infos; Type: VIEW; Schema: niveaux; Owner: -
--

CREATE VIEW point_suivi_infos AS
 SELECT point_suivi.id_point_suivi,
    point_suivi.point_suivi,
    point_suivi.type_suivi,
    point_suivi.type_point,
    point_suivi.type_releve,
    point_suivi.structure_gestionnaire,
    point_suivi.bdc_id_structure_gestionnaire,
    point_suivi.carthage_cours_eau,
    point_suivi.carthage_id_cours_eau,
    point_suivi.departement,
    point_suivi.insee_commune,
    point_suivi.code_station,
    point_suivi.nom_station_officiel,
    point_suivi.lien_vigicrue,
    point_suivi.lien_banquehydro,
    point_suivi.lien_infoclimat,
    point_suivi.lien_autre,
    point_suivi.alias,
    point_suivi.geom,
        CASE
            WHEN ((point_suivi.type_point)::text = 'hauteur'::text) THEN 'm'::text
            WHEN ((point_suivi.type_point)::text = 'debit'::text) THEN 'm3/s'::text
            WHEN ((point_suivi.type_point)::text = 'pluvio'::text) THEN 'mm'::text
            WHEN ((point_suivi.type_point)::text = 'seuil'::text) THEN ''::text
            ELSE NULL::text
        END AS unite,
        CASE
            WHEN ((point_suivi.type_point)::text = 'hauteur'::text) THEN 'hauteur'::text
            WHEN ((point_suivi.type_point)::text = 'debit'::text) THEN 'débit'::text
            WHEN ((point_suivi.type_point)::text = 'pluvio'::text) THEN 'cumul pluviométrique horaire'::text
            WHEN ((point_suivi.type_point)::text = 'seuil'::text) THEN 'seuil'::text
            ELSE NULL::text
        END AS element_mesure,
    public.st_astext(point_suivi.geom) AS geom_text,
    public.st_astext(public.st_transform(point_suivi.geom, 4326)) AS geom_text4326
   FROM point_suivi;


--
-- TOC entry 354 (class 1259 OID 101883067)
-- Name: notification_infos; Type: VIEW; Schema: niveaux; Owner: -
--

CREATE VIEW notification_infos AS
 SELECT n.id_notification,
    n.type_notification,
    n.seuil,
    n.montant_descendant,
    n.date_creation,
    n.active,
    n.fonction,
    n.fonction_parametres,
    u.id_user,
    u.username,
    u.prenom,
    u.nom,
    u.mail,
    u.telephone,
    p.id_point_suivi,
    p.point_suivi,
    p.type_suivi,
    p.type_point,
    p.type_releve,
    p.structure_gestionnaire,
    p.carthage_id_cours_eau,
    p.carthage_cours_eau,
    p.unite,
    p.element_mesure
   FROM ((notification n
     LEFT JOIN utilisateur u ON ((u.id_user = n.id_user)))
     LEFT JOIN point_suivi_infos p ON ((p.id_point_suivi = n.id_point_suivi)));


--
-- TOC entry 355 (class 1259 OID 101883072)
-- Name: notification_envoi_infos; Type: VIEW; Schema: niveaux; Owner: -
--

CREATE VIEW notification_envoi_infos AS
 SELECT e.id_notification,
    e.id_envoi_notification,
    e.date_envoi_notification,
    e.id_releve,
    e.statut,
    e.contenu_notification,
    e.releve_valeur,
    e.releve_valeur_date,
    i.type_notification,
    i.seuil,
    i.montant_descendant,
    i.type_suivi,
    i.type_point,
    i.type_releve,
    i.username,
    i.point_suivi
   FROM (notification_envoi e
     LEFT JOIN notification_infos i ON ((i.id_notification = e.id_notification)));


--
-- TOC entry 334 (class 1259 OID 101747980)
-- Name: notification_id_notification_seq; Type: SEQUENCE; Schema: niveaux; Owner: -
--

CREATE SEQUENCE notification_id_notification_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4122 (class 0 OID 0)
-- Dependencies: 334
-- Name: notification_id_notification_seq; Type: SEQUENCE OWNED BY; Schema: niveaux; Owner: -
--

ALTER SEQUENCE notification_id_notification_seq OWNED BY notification.id_notification;


--
-- TOC entry 335 (class 1259 OID 101747982)
-- Name: parametres; Type: TABLE; Schema: niveaux; Owner: -
--

CREATE TABLE parametres (
    id_parametre integer NOT NULL,
    parametre character varying,
    alias character varying,
    type character varying,
    valeur text,
    ordre integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 4123 (class 0 OID 0)
-- Dependencies: 335
-- Name: TABLE parametres; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON TABLE parametres IS 'liste des paramètres de l''application';


--
-- TOC entry 336 (class 1259 OID 101747989)
-- Name: parametres_id_parametre_seq; Type: SEQUENCE; Schema: niveaux; Owner: -
--

CREATE SEQUENCE parametres_id_parametre_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4124 (class 0 OID 0)
-- Dependencies: 336
-- Name: parametres_id_parametre_seq; Type: SEQUENCE OWNED BY; Schema: niveaux; Owner: -
--

ALTER SEQUENCE parametres_id_parametre_seq OWNED BY parametres.id_parametre;


--
-- TOC entry 337 (class 1259 OID 101747991)
-- Name: point_suivi_id_point_suivi_seq; Type: SEQUENCE; Schema: niveaux; Owner: -
--

CREATE SEQUENCE point_suivi_id_point_suivi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4125 (class 0 OID 0)
-- Dependencies: 337
-- Name: point_suivi_id_point_suivi_seq; Type: SEQUENCE OWNED BY; Schema: niveaux; Owner: -
--

ALTER SEQUENCE point_suivi_id_point_suivi_seq OWNED BY point_suivi.id_point_suivi;


--
-- TOC entry 338 (class 1259 OID 101747993)
-- Name: releve; Type: TABLE; Schema: niveaux; Owner: -
--

CREATE TABLE releve (
    id_releve integer NOT NULL,
    id_user integer,
    date_releve timestamp with time zone DEFAULT now(),
    type_releve character varying,
    type_suivi character varying,
    id_tache integer DEFAULT 0
);


--
-- TOC entry 4126 (class 0 OID 0)
-- Dependencies: 338
-- Name: COLUMN releve.type_releve; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN releve.type_releve IS 'auto,manuel';


--
-- TOC entry 4127 (class 0 OID 0)
-- Dependencies: 338
-- Name: COLUMN releve.type_suivi; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN releve.type_suivi IS 'valeur,seuil';


--
-- TOC entry 4128 (class 0 OID 0)
-- Dependencies: 338
-- Name: COLUMN releve.id_tache; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN releve.id_tache IS 'id de la tache qui a créé ce relevé (pour un relevé auto)';


--
-- TOC entry 339 (class 1259 OID 101748001)
-- Name: releve_id_releve_seq; Type: SEQUENCE; Schema: niveaux; Owner: -
--

CREATE SEQUENCE releve_id_releve_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4129 (class 0 OID 0)
-- Dependencies: 339
-- Name: releve_id_releve_seq; Type: SEQUENCE OWNED BY; Schema: niveaux; Owner: -
--

ALTER SEQUENCE releve_id_releve_seq OWNED BY releve.id_releve;


--
-- TOC entry 351 (class 1259 OID 101748166)
-- Name: releve_infos; Type: VIEW; Schema: niveaux; Owner: -
--

CREATE VIEW releve_infos AS
 SELECT r.id_releve,
    r.type_releve,
    r.type_suivi,
    r.date_releve,
    u.id_user,
    u.prenom,
    u.nom,
    u.bdc_id_structure
   FROM (releve r
     LEFT JOIN utilisateur u ON ((u.id_user = r.id_user)));


--
-- TOC entry 340 (class 1259 OID 101748003)
-- Name: releve_valeur; Type: TABLE; Schema: niveaux; Owner: -
--

CREATE TABLE releve_valeur (
    id_valeur integer NOT NULL,
    id_releve integer NOT NULL,
    date_valeur timestamp with time zone,
    id_point_suivi integer,
    valeur numeric
);


--
-- TOC entry 341 (class 1259 OID 101748009)
-- Name: seuil; Type: TABLE; Schema: niveaux; Owner: -
--

CREATE TABLE seuil (
    id_seuil integer NOT NULL,
    id_point_suivi integer,
    nom character varying,
    date_seuil timestamp with time zone,
    valeur numeric,
    style json,
    commentaire text
);


--
-- TOC entry 4130 (class 0 OID 0)
-- Dependencies: 341
-- Name: TABLE seuil; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON TABLE seuil IS 'liste de seuils pour un point de suivi (crue de références...)';


--
-- TOC entry 342 (class 1259 OID 101748015)
-- Name: seuil_id_seuil_seq; Type: SEQUENCE; Schema: niveaux; Owner: -
--

CREATE SEQUENCE seuil_id_seuil_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4131 (class 0 OID 0)
-- Dependencies: 342
-- Name: seuil_id_seuil_seq; Type: SEQUENCE OWNED BY; Schema: niveaux; Owner: -
--

ALTER SEQUENCE seuil_id_seuil_seq OWNED BY seuil.id_seuil;


--
-- TOC entry 343 (class 1259 OID 101748017)
-- Name: tache_executee; Type: TABLE; Schema: niveaux; Owner: -
--

CREATE TABLE tache_executee (
    id_tache_executee integer NOT NULL,
    id_tache integer,
    statut character varying,
    date_execution timestamp with time zone
);


--
-- TOC entry 4132 (class 0 OID 0)
-- Dependencies: 343
-- Name: TABLE tache_executee; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON TABLE tache_executee IS 'liste des taches planifiées executées';


--
-- TOC entry 4133 (class 0 OID 0)
-- Dependencies: 343
-- Name: COLUMN tache_executee.statut; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN tache_executee.statut IS 'succes,echec';


--
-- TOC entry 344 (class 1259 OID 101748023)
-- Name: tache_executee_id_tache_executee_seq; Type: SEQUENCE; Schema: niveaux; Owner: -
--

CREATE SEQUENCE tache_executee_id_tache_executee_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4134 (class 0 OID 0)
-- Dependencies: 344
-- Name: tache_executee_id_tache_executee_seq; Type: SEQUENCE OWNED BY; Schema: niveaux; Owner: -
--

ALTER SEQUENCE tache_executee_id_tache_executee_seq OWNED BY tache_executee.id_tache_executee;


--
-- TOC entry 345 (class 1259 OID 101748025)
-- Name: tache_planifiee; Type: TABLE; Schema: niveaux; Owner: -
--

CREATE TABLE tache_planifiee (
    id_tache integer NOT NULL,
    tache character varying,
    frequence_execution integer,
    date_derniere_execution timestamp with time zone,
    statut_derniere_execution character varying,
    ligne_commande character varying,
    parametres_ligne_commande text,
    ids_point_suivi integer[],
    ordre integer,
    actif boolean
);


--
-- TOC entry 4135 (class 0 OID 0)
-- Dependencies: 345
-- Name: TABLE tache_planifiee; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON TABLE tache_planifiee IS 'taches cron de relevés de valeurs';


--
-- TOC entry 4136 (class 0 OID 0)
-- Dependencies: 345
-- Name: COLUMN tache_planifiee.frequence_execution; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN tache_planifiee.frequence_execution IS 'frequence d''execution en minutes';


--
-- TOC entry 4137 (class 0 OID 0)
-- Dependencies: 345
-- Name: COLUMN tache_planifiee.statut_derniere_execution; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN tache_planifiee.statut_derniere_execution IS 'succes,echec';


--
-- TOC entry 4138 (class 0 OID 0)
-- Dependencies: 345
-- Name: COLUMN tache_planifiee.ligne_commande; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN tache_planifiee.ligne_commande IS 'commande à executer';


--
-- TOC entry 4139 (class 0 OID 0)
-- Dependencies: 345
-- Name: COLUMN tache_planifiee.ids_point_suivi; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN tache_planifiee.ids_point_suivi IS 'liste des points de suivi concernés par cette tâche';


--
-- TOC entry 4140 (class 0 OID 0)
-- Dependencies: 345
-- Name: COLUMN tache_planifiee.ordre; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON COLUMN tache_planifiee.ordre IS 'ordre si déclenchement de plusieurs tâches simultanées';


--
-- TOC entry 346 (class 1259 OID 101748031)
-- Name: tache_planifiee_id_tache_seq; Type: SEQUENCE; Schema: niveaux; Owner: -
--

CREATE SEQUENCE tache_planifiee_id_tache_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4141 (class 0 OID 0)
-- Dependencies: 346
-- Name: tache_planifiee_id_tache_seq; Type: SEQUENCE OWNED BY; Schema: niveaux; Owner: -
--

ALTER SEQUENCE tache_planifiee_id_tache_seq OWNED BY tache_planifiee.id_tache;


--
-- TOC entry 347 (class 1259 OID 101748033)
-- Name: utilisateur_connexion_id_connexion_seq; Type: SEQUENCE; Schema: niveaux; Owner: -
--

CREATE SEQUENCE utilisateur_connexion_id_connexion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4142 (class 0 OID 0)
-- Dependencies: 347
-- Name: utilisateur_connexion_id_connexion_seq; Type: SEQUENCE OWNED BY; Schema: niveaux; Owner: -
--

ALTER SEQUENCE utilisateur_connexion_id_connexion_seq OWNED BY utilisateur_connexion.id_connexion;


--
-- TOC entry 348 (class 1259 OID 101748035)
-- Name: utilisateurs_id_utilisateur_seq; Type: SEQUENCE; Schema: niveaux; Owner: -
--

CREATE SEQUENCE utilisateurs_id_utilisateur_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4143 (class 0 OID 0)
-- Dependencies: 348
-- Name: utilisateurs_id_utilisateur_seq; Type: SEQUENCE OWNED BY; Schema: niveaux; Owner: -
--

ALTER SEQUENCE utilisateurs_id_utilisateur_seq OWNED BY utilisateur.id_user;


--
-- TOC entry 352 (class 1259 OID 101748170)
-- Name: valeur_infos; Type: VIEW; Schema: niveaux; Owner: -
--

CREATE VIEW valeur_infos AS
 SELECT v.id_valeur,
    v.valeur,
    v.date_valeur,
    v.id_point_suivi,
    r.id_releve,
    r.date_releve,
    r.type_releve,
    r.type_suivi,
    u.id_user,
    u.username,
    u.prenom,
    u.nom,
    u.bdc_id_contact,
    u.bdc_id_structure
   FROM ((releve_valeur v
     LEFT JOIN releve r ON ((r.id_releve = v.id_releve)))
     LEFT JOIN utilisateur u ON ((u.id_user = r.id_user)));


--
-- TOC entry 349 (class 1259 OID 101748037)
-- Name: valeur_releve_id_valeur_releve_seq; Type: SEQUENCE; Schema: niveaux; Owner: -
--

CREATE SEQUENCE valeur_releve_id_valeur_releve_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4144 (class 0 OID 0)
-- Dependencies: 349
-- Name: valeur_releve_id_valeur_releve_seq; Type: SEQUENCE OWNED BY; Schema: niveaux; Owner: -
--

ALTER SEQUENCE valeur_releve_id_valeur_releve_seq OWNED BY releve_valeur.id_valeur;


--
-- TOC entry 3868 (class 2604 OID 101748039)
-- Name: l_user_point_suivi id_l_user_point_suivi; Type: DEFAULT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY l_user_point_suivi ALTER COLUMN id_l_user_point_suivi SET DEFAULT nextval('l_user_point_suivi_id_l_user_point_suivi_seq'::regclass);


--
-- TOC entry 3869 (class 2604 OID 101748040)
-- Name: l_user_point_suivi_favori id_l_user_pt_suivi_favori; Type: DEFAULT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY l_user_point_suivi_favori ALTER COLUMN id_l_user_pt_suivi_favori SET DEFAULT nextval('l_user_point_suivi_favori_id_l_user_pt_suivi_favori_seq'::regclass);


--
-- TOC entry 3870 (class 2604 OID 101748041)
-- Name: notification id_notification; Type: DEFAULT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY notification ALTER COLUMN id_notification SET DEFAULT nextval('notification_id_notification_seq'::regclass);


--
-- TOC entry 3866 (class 2604 OID 101748042)
-- Name: notification_envoi id_envoi_notification; Type: DEFAULT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY notification_envoi ALTER COLUMN id_envoi_notification SET DEFAULT nextval('envoi_notification_id_envoi_notification_seq'::regclass);


--
-- TOC entry 3875 (class 2604 OID 101748043)
-- Name: parametres id_parametre; Type: DEFAULT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY parametres ALTER COLUMN id_parametre SET DEFAULT nextval('parametres_id_parametre_seq'::regclass);


--
-- TOC entry 3874 (class 2604 OID 101748044)
-- Name: point_suivi id_point_suivi; Type: DEFAULT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY point_suivi ALTER COLUMN id_point_suivi SET DEFAULT nextval('point_suivi_id_point_suivi_seq'::regclass);


--
-- TOC entry 3877 (class 2604 OID 101748045)
-- Name: releve id_releve; Type: DEFAULT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY releve ALTER COLUMN id_releve SET DEFAULT nextval('releve_id_releve_seq'::regclass);


--
-- TOC entry 3880 (class 2604 OID 101748046)
-- Name: releve_valeur id_valeur; Type: DEFAULT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY releve_valeur ALTER COLUMN id_valeur SET DEFAULT nextval('valeur_releve_id_valeur_releve_seq'::regclass);


--
-- TOC entry 3881 (class 2604 OID 101748047)
-- Name: seuil id_seuil; Type: DEFAULT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY seuil ALTER COLUMN id_seuil SET DEFAULT nextval('seuil_id_seuil_seq'::regclass);


--
-- TOC entry 3882 (class 2604 OID 101748048)
-- Name: tache_executee id_tache_executee; Type: DEFAULT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY tache_executee ALTER COLUMN id_tache_executee SET DEFAULT nextval('tache_executee_id_tache_executee_seq'::regclass);


--
-- TOC entry 3883 (class 2604 OID 101748049)
-- Name: tache_planifiee id_tache; Type: DEFAULT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY tache_planifiee ALTER COLUMN id_tache SET DEFAULT nextval('tache_planifiee_id_tache_seq'::regclass);


--
-- TOC entry 3859 (class 2604 OID 101748050)
-- Name: utilisateur id_user; Type: DEFAULT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY utilisateur ALTER COLUMN id_user SET DEFAULT nextval('utilisateurs_id_utilisateur_seq'::regclass);


--
-- TOC entry 3864 (class 2604 OID 101748051)
-- Name: utilisateur_connexion id_connexion; Type: DEFAULT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY utilisateur_connexion ALTER COLUMN id_connexion SET DEFAULT nextval('utilisateur_connexion_id_connexion_seq'::regclass);


--
-- TOC entry 4145 (class 0 OID 0)
-- Dependencies: 327
-- Name: envoi_notification_id_envoi_notification_seq; Type: SEQUENCE SET; Schema: niveaux; Owner: -
--

SELECT pg_catalog.setval('envoi_notification_id_envoi_notification_seq', 17, true);


--
-- TOC entry 4077 (class 0 OID 101747952)
-- Dependencies: 328
-- Data for Name: l_user_point_suivi; Type: TABLE DATA; Schema: niveaux; Owner: -
--

INSERT INTO l_user_point_suivi VALUES (1, 1, 'ROLE_LECTEUR', '{0}');
INSERT INTO l_user_point_suivi VALUES (2, 1, 'ROLE_CONTRIB', '{0}');
INSERT INTO l_user_point_suivi VALUES (3, 1, 'ROLE_GESTIONNAIRE', '{0}');


--
-- TOC entry 4078 (class 0 OID 101747958)
-- Dependencies: 329
-- Data for Name: l_user_point_suivi_favori; Type: TABLE DATA; Schema: niveaux; Owner: -
--



--
-- TOC entry 4146 (class 0 OID 0)
-- Dependencies: 330
-- Name: l_user_point_suivi_favori_id_l_user_pt_suivi_favori_seq; Type: SEQUENCE SET; Schema: niveaux; Owner: -
--

SELECT pg_catalog.setval('l_user_point_suivi_favori_id_l_user_pt_suivi_favori_seq', 1, true);


--
-- TOC entry 4147 (class 0 OID 0)
-- Dependencies: 331
-- Name: l_user_point_suivi_id_l_user_point_suivi_seq; Type: SEQUENCE SET; Schema: niveaux; Owner: -
--

SELECT pg_catalog.setval('l_user_point_suivi_id_l_user_point_suivi_seq', 27, true);


--
-- TOC entry 4081 (class 0 OID 101747965)
-- Dependencies: 332
-- Data for Name: notification; Type: TABLE DATA; Schema: niveaux; Owner: -
--



--
-- TOC entry 4075 (class 0 OID 101747943)
-- Dependencies: 326
-- Data for Name: notification_envoi; Type: TABLE DATA; Schema: niveaux; Owner: -
--



--
-- TOC entry 4148 (class 0 OID 0)
-- Dependencies: 334
-- Name: notification_id_notification_seq; Type: SEQUENCE SET; Schema: niveaux; Owner: -
--

SELECT pg_catalog.setval('notification_id_notification_seq', 4, true);


--
-- TOC entry 4084 (class 0 OID 101747982)
-- Dependencies: 335
-- Data for Name: parametres; Type: TABLE DATA; Schema: niveaux; Owner: -
--

INSERT INTO parametres VALUES (3, 'Nom de l''appli', 'BRAND', 'text', 'Niveaux', 1);
INSERT INTO parametres VALUES (13, 'Structure gestionnaire de l''appli', 'BRANDSTRUCTURE', 'text', 'Structure', 2);
INSERT INTO parametres VALUES (1, 'Adresse web de l''appli (avec http://...)', 'WEBURL', 'text', 'http://...', 3);
INSERT INTO parametres VALUES (12, 'Message page d''accueil', 'ACCUEIL', 'textarea', '<h1>Niv''Eaux</h1>
<h3>Outil de partage d''informations&nbsp;sur les crues</h3>', 32);
INSERT INTO parametres VALUES (4, 'Timezone', 'TIMEZONE', 'text', 'Europe/Paris', 40);
INSERT INTO parametres VALUES (5, 'Durée par défaut des tableaux de suivi', 'DEFAULTDURATION', 'text', '36hours', 50);
INSERT INTO parametres VALUES (6, 'Durée par défaut du cumul des pluies', 'DEFAULTDURATIONTOTALRAIN', 'integer', '6', 60);
INSERT INTO parametres VALUES (7, 'E-mail de l''administrateur du site', 'MAILADMIN', 'email', 'mail@mail.com', 70);
INSERT INTO parametres VALUES (16, 'E-mail de l''administrateur 2 du site', 'MAILADMIN2', 'email', 'mail@mail.com', 71);
INSERT INTO parametres VALUES (8, 'E-mail de l''expediteur des notifications mail', 'MAILEXP', 'email', 'mail@mail.com', 80);
INSERT INTO parametres VALUES (9, 'Nom de l''expéditeur des notifications mail', 'NAMEEXP', 'email', 'votre structure', 90);
INSERT INTO parametres VALUES (10, 'Conditions d''utilisation complètes', 'CONDITIONSCOMPLETES', 'textarea', '<h1>Conditions g&eacute;n&eacute;rales d''utilisation - Version du xxx</h1>
<p><span style="text-decoration: underline;"><strong>Merci de lire les conditions d''utilisation ci-dessous (ce texte apparait car il s''agit de votre premi&egrave;re connexion) :</strong></span></p>
<h2><strong>Avertissement</strong></h2>
<p>Conditions d''utilisation &agrave; personnaliser</p>', 100);
INSERT INTO parametres VALUES (11, 'Conditions d''utilisation bandeau', 'CONDITIONSBANDEAU', 'textarea', '<p>Message d''avertissement sur les conditions d''utilisation &agrave; personnaliser</p>', 110);
INSERT INTO parametres VALUES (14, 'Durée de contrôle du nombre de SMS envoyés', 'SMSDELAY', 'text', '24hours', 200);
INSERT INTO parametres VALUES (15, 'Nombre maximum de SMS envoyés à  chaque utilisateur dans la durée de contrôle', 'SMSMAX', 'integer', '5', 201);
INSERT INTO parametres VALUES (2, 'Couches de la carte (geojson) sous la forme : fichier1/color*#333333_weight*3px;fichier2/color*#333399_weight*1px', 'LAYERS', 'text', 'carthage2008sbv.geojson/''color'':''#006633'',''weight'':''3px'';carthage2008cdopcpx.geojson/''color'':''#003399'',''weight'':''2px''', 300);
INSERT INTO parametres VALUES (17, 'Centre de la carte en WGS84 - ex : 46.92731, -1.2', 'MAPCENTER', 'text', '47.12,-1.54', 400);
INSERT INTO parametres VALUES (18, 'Zoom par défaut de la carte (entre 1 et 20)', 'MAPZOOM', 'integer', '11', 401);


--
-- TOC entry 4149 (class 0 OID 0)
-- Dependencies: 336
-- Name: parametres_id_parametre_seq; Type: SEQUENCE SET; Schema: niveaux; Owner: -
--

SELECT pg_catalog.setval('parametres_id_parametre_seq', 9, true);


--
-- TOC entry 4082 (class 0 OID 101747974)
-- Dependencies: 333
-- Data for Name: point_suivi; Type: TABLE DATA; Schema: niveaux; Owner: -
--

INSERT INTO point_suivi VALUES (2, 'Nantes-Atlantique (Bouguenais)', 'valeur', 'pluvio', 'manuel', 'InfoClimat.fr / Météo-France', 0, NULL, NULL, '44', '44020', '44020001', 'nantes-atlantique-bouguenais', '01010000206A080000F86038BA366C1541BEBCA14AC57D5941', 'nantes-atlantique', NULL, NULL, 'https://www.infoclimat.fr/observations-meteo/temps-reel/nantes-atlantique/07222.html', NULL);
INSERT INTO point_suivi VALUES (4, 'Station manuelle test', 'valeur', 'hauteur', 'manuel', 'Test', 0, NULL, NULL, NULL, NULL, NULL, 'test', '01010000206A08000000000000D0E215410000008019805941', NULL, NULL, NULL, NULL, NULL);
INSERT INTO point_suivi VALUES (1, 'La Loire à Nantes', 'valeur', 'hauteur', 'auto', 'DREAL Pays de la Loire', 0, 'La Loire', NULL, '44', '44', 'M800001010', 'loire-nantes', '01010000206A08000000000000F0A5154100000000F5835941', 'Nantes [Pont Anne de Bretagne]', 'http://www.vigicrues.gouv.fr/niveau3.php?CdStationHydro=M800001010&CdEntVigiCru=9&typegraphe=h&AffProfondeur=72&AffRef=auto&AffPrevi=non&nbrstations=1&ong=1', NULL, NULL, NULL);
INSERT INTO point_suivi VALUES (3, 'Remouillé (Maine) (débits)', 'valeur', 'debit', 'auto', 'DREAL Pays de la Loire', 0, 'La Maine', NULL, '44', '44142', 'M745301010', 'remouille-maine-debits', '01010000206A08000000000000747B1641000000408D725941', 'Remouillé (Maine)', 'https://www.vigicrues.gouv.fr/niv3-station.php?CdEntVigiCru=9&CdStationHydro=M745301010&GrdSerie=Q&ZoomInitial=3', NULL, NULL, NULL);


--
-- TOC entry 4150 (class 0 OID 0)
-- Dependencies: 337
-- Name: point_suivi_id_point_suivi_seq; Type: SEQUENCE SET; Schema: niveaux; Owner: -
--

SELECT pg_catalog.setval('point_suivi_id_point_suivi_seq', 5, true);


--
-- TOC entry 4087 (class 0 OID 101747993)
-- Dependencies: 338
-- Data for Name: releve; Type: TABLE DATA; Schema: niveaux; Owner: -
--



--
-- TOC entry 4151 (class 0 OID 0)
-- Dependencies: 339
-- Name: releve_id_releve_seq; Type: SEQUENCE SET; Schema: niveaux; Owner: -
--

SELECT pg_catalog.setval('releve_id_releve_seq', 49, true);


--
-- TOC entry 4089 (class 0 OID 101748003)
-- Dependencies: 340
-- Data for Name: releve_valeur; Type: TABLE DATA; Schema: niveaux; Owner: -
--



--
-- TOC entry 4090 (class 0 OID 101748009)
-- Dependencies: 341
-- Data for Name: seuil; Type: TABLE DATA; Schema: niveaux; Owner: -
--



--
-- TOC entry 4152 (class 0 OID 0)
-- Dependencies: 342
-- Name: seuil_id_seuil_seq; Type: SEQUENCE SET; Schema: niveaux; Owner: -
--

SELECT pg_catalog.setval('seuil_id_seuil_seq', 1, false);


--
-- TOC entry 4092 (class 0 OID 101748017)
-- Dependencies: 343
-- Data for Name: tache_executee; Type: TABLE DATA; Schema: niveaux; Owner: -
--



--
-- TOC entry 4153 (class 0 OID 0)
-- Dependencies: 344
-- Name: tache_executee_id_tache_executee_seq; Type: SEQUENCE SET; Schema: niveaux; Owner: -
--

SELECT pg_catalog.setval('tache_executee_id_tache_executee_seq', 51, true);


--
-- TOC entry 4094 (class 0 OID 101748025)
-- Dependencies: 345
-- Data for Name: tache_planifiee; Type: TABLE DATA; Schema: niveaux; Owner: -
--

INSERT INTO tache_planifiee VALUES (1, 'controle notification', 1, '2020-04-16 10:28:41+02', 'succes', 'controle-notification', '', '{}', 99, true);
INSERT INTO tache_planifiee VALUES (4, 'vigicrue json - Loire Nantes', 1, '2020-04-16 10:28:40+02', 'succes', 'import-vigicrue-json', 'https://www.vigicrues.gouv.fr/services/observations.json/index.php?CdStationHydro=M800001010&GrdSerie=H&FormatSortie=json', '{1}', 1, true);
INSERT INTO tache_planifiee VALUES (3, 'API hydrométrie json - Saint Lambert débit', 1, '2020-04-16 10:42:11+02', 'succes', 'import-api-hydrometrie-json', 'https://hubeau.eaufrance.fr/api/v1/hydrometrie/observations_tr?code_entite=M745301010&grandeur_hydro=Q', '{3}', 3, true);
INSERT INTO tache_planifiee VALUES (2, 'infoclimat - nantes atlantique', 1, '2020-04-16 10:28:41+02', 'succes', 'import-infoclimat', 'http://www.infoclimat.fr/observations-meteo/temps-reel/nantes-atlantique/07222.html', '{2}', 2, true);


--
-- TOC entry 4154 (class 0 OID 0)
-- Dependencies: 346
-- Name: tache_planifiee_id_tache_seq; Type: SEQUENCE SET; Schema: niveaux; Owner: -
--

SELECT pg_catalog.setval('tache_planifiee_id_tache_seq', 1, true);


--
-- TOC entry 4073 (class 0 OID 101747926)
-- Dependencies: 324
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: niveaux; Owner: -
--

INSERT INTO utilisateur VALUES (0, 'moulinettePHP', 'Relevé', 'automatique', 0, 0, NULL, NULL, NULL, 'ROLE_USER', NULL, NULL, 0);
INSERT INTO utilisateur VALUES (1, 'admin', 'admin', 'admin', 0, 0, 'nhDr7OyKlXQju+Ge/WKGrPQ9lPBSUFfpK+B1xqx/+8zLZqRNX0+5G1zBQklXUFy86lCpkAofsExlXiorUcKSNQ==', 'admin@admin', NULL, 'ROLE_USER,ROLE_ADMIN,ROLE_SUPERADMIN', '2018-05-24', '2018-05-24 10:59:20.910324+02', 1);


--
-- TOC entry 4074 (class 0 OID 101747935)
-- Dependencies: 325
-- Data for Name: utilisateur_connexion; Type: TABLE DATA; Schema: niveaux; Owner: -
--

INSERT INTO utilisateur_connexion VALUES (1, '2020-04-16 11:15:49.403926+02', 12, 'appli_web');


--
-- TOC entry 4155 (class 0 OID 0)
-- Dependencies: 347
-- Name: utilisateur_connexion_id_connexion_seq; Type: SEQUENCE SET; Schema: niveaux; Owner: -
--

SELECT pg_catalog.setval('utilisateur_connexion_id_connexion_seq', 12, true);


--
-- TOC entry 4156 (class 0 OID 0)
-- Dependencies: 348
-- Name: utilisateurs_id_utilisateur_seq; Type: SEQUENCE SET; Schema: niveaux; Owner: -
--

SELECT pg_catalog.setval('utilisateurs_id_utilisateur_seq', 2, true);


--
-- TOC entry 4157 (class 0 OID 0)
-- Dependencies: 349
-- Name: valeur_releve_id_valeur_releve_seq; Type: SEQUENCE SET; Schema: niveaux; Owner: -
--

SELECT pg_catalog.setval('valeur_releve_id_valeur_releve_seq', 2497, true);


--
-- TOC entry 3891 (class 2606 OID 101748053)
-- Name: notification_envoi envoi_notification_pkey; Type: CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY notification_envoi
    ADD CONSTRAINT envoi_notification_pkey PRIMARY KEY (id_envoi_notification);


--
-- TOC entry 3895 (class 2606 OID 101748055)
-- Name: l_user_point_suivi_favori l_user_point_suivi_favori_id_user_id_point_suivi_key; Type: CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY l_user_point_suivi_favori
    ADD CONSTRAINT l_user_point_suivi_favori_id_user_id_point_suivi_key UNIQUE (id_user, id_point_suivi);


--
-- TOC entry 3897 (class 2606 OID 101748057)
-- Name: l_user_point_suivi_favori l_user_point_suivi_favori_pkey; Type: CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY l_user_point_suivi_favori
    ADD CONSTRAINT l_user_point_suivi_favori_pkey PRIMARY KEY (id_l_user_pt_suivi_favori);


--
-- TOC entry 3893 (class 2606 OID 101748059)
-- Name: l_user_point_suivi l_utilisateur_point_suivi_pkey; Type: CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY l_user_point_suivi
    ADD CONSTRAINT l_utilisateur_point_suivi_pkey PRIMARY KEY (id_l_user_point_suivi);


--
-- TOC entry 3899 (class 2606 OID 101748061)
-- Name: notification notification_pkey; Type: CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT notification_pkey PRIMARY KEY (id_notification);


--
-- TOC entry 3905 (class 2606 OID 101748063)
-- Name: parametres parametres_pkey; Type: CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY parametres
    ADD CONSTRAINT parametres_pkey PRIMARY KEY (id_parametre);


--
-- TOC entry 3901 (class 2606 OID 101748065)
-- Name: point_suivi point_suivi_alias_key; Type: CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY point_suivi
    ADD CONSTRAINT point_suivi_alias_key UNIQUE (alias);


--
-- TOC entry 3903 (class 2606 OID 101748067)
-- Name: point_suivi point_suivi_pkey; Type: CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY point_suivi
    ADD CONSTRAINT point_suivi_pkey PRIMARY KEY (id_point_suivi);


--
-- TOC entry 3907 (class 2606 OID 101748069)
-- Name: releve releve_pkey; Type: CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY releve
    ADD CONSTRAINT releve_pkey PRIMARY KEY (id_releve);


--
-- TOC entry 3913 (class 2606 OID 101748071)
-- Name: seuil seuil_pkey; Type: CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY seuil
    ADD CONSTRAINT seuil_pkey PRIMARY KEY (id_seuil);


--
-- TOC entry 3915 (class 2606 OID 101748073)
-- Name: tache_executee tache_executee_pkey; Type: CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY tache_executee
    ADD CONSTRAINT tache_executee_pkey PRIMARY KEY (id_tache_executee);


--
-- TOC entry 3917 (class 2606 OID 101748075)
-- Name: tache_planifiee tache_planifiee_pkey; Type: CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY tache_planifiee
    ADD CONSTRAINT tache_planifiee_pkey PRIMARY KEY (id_tache);


--
-- TOC entry 3909 (class 2606 OID 101748077)
-- Name: releve_valeur uniq_valeur_datevaleur_idptsuivi_idreleve; Type: CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY releve_valeur
    ADD CONSTRAINT uniq_valeur_datevaleur_idptsuivi_idreleve UNIQUE (valeur, date_valeur, id_point_suivi, id_releve);


--
-- TOC entry 4158 (class 0 OID 0)
-- Dependencies: 3909
-- Name: CONSTRAINT uniq_valeur_datevaleur_idptsuivi_idreleve ON releve_valeur; Type: COMMENT; Schema: niveaux; Owner: -
--

COMMENT ON CONSTRAINT uniq_valeur_datevaleur_idptsuivi_idreleve ON releve_valeur IS 'attention laisse passer des valeurs identiques de relevés différents (mais de même type)';


--
-- TOC entry 3889 (class 2606 OID 101748079)
-- Name: utilisateur_connexion utilisateur_connexion_pkey; Type: CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY utilisateur_connexion
    ADD CONSTRAINT utilisateur_connexion_pkey PRIMARY KEY (id_connexion);


--
-- TOC entry 3885 (class 2606 OID 101748081)
-- Name: utilisateur utilisateurs_pkey; Type: CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateurs_pkey PRIMARY KEY (id_user);


--
-- TOC entry 3887 (class 2606 OID 101748083)
-- Name: utilisateur utilisateurs_username_key; Type: CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateurs_username_key UNIQUE (username);


--
-- TOC entry 3911 (class 2606 OID 101748085)
-- Name: releve_valeur valeur_releve_pkey; Type: CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY releve_valeur
    ADD CONSTRAINT valeur_releve_pkey PRIMARY KEY (id_valeur);


--
-- TOC entry 3920 (class 2606 OID 101748086)
-- Name: notification_envoi envoi_notification_id_notification_fkey; Type: FK CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY notification_envoi
    ADD CONSTRAINT envoi_notification_id_notification_fkey FOREIGN KEY (id_notification) REFERENCES notification(id_notification);


--
-- TOC entry 3923 (class 2606 OID 101748091)
-- Name: l_user_point_suivi_favori l_user_point_suivi_favori_id_point_suivi_fkey; Type: FK CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY l_user_point_suivi_favori
    ADD CONSTRAINT l_user_point_suivi_favori_id_point_suivi_fkey FOREIGN KEY (id_point_suivi) REFERENCES point_suivi(id_point_suivi);


--
-- TOC entry 3922 (class 2606 OID 101748096)
-- Name: l_user_point_suivi_favori l_user_point_suivi_favori_id_user_fkey; Type: FK CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY l_user_point_suivi_favori
    ADD CONSTRAINT l_user_point_suivi_favori_id_user_fkey FOREIGN KEY (id_user) REFERENCES utilisateur(id_user);


--
-- TOC entry 3921 (class 2606 OID 101748101)
-- Name: l_user_point_suivi l_user_point_suivi_id_user_fkey; Type: FK CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY l_user_point_suivi
    ADD CONSTRAINT l_user_point_suivi_id_user_fkey FOREIGN KEY (id_user) REFERENCES utilisateur(id_user);


--
-- TOC entry 3919 (class 2606 OID 101748106)
-- Name: notification_envoi notification_envoi_id_releve_fkey; Type: FK CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY notification_envoi
    ADD CONSTRAINT notification_envoi_id_releve_fkey FOREIGN KEY (id_releve) REFERENCES releve(id_releve);


--
-- TOC entry 3925 (class 2606 OID 101748111)
-- Name: notification notification_id_point_suivi_fkey; Type: FK CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT notification_id_point_suivi_fkey FOREIGN KEY (id_point_suivi) REFERENCES point_suivi(id_point_suivi);


--
-- TOC entry 3924 (class 2606 OID 101748116)
-- Name: notification notification_id_user_fkey; Type: FK CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT notification_id_user_fkey FOREIGN KEY (id_user) REFERENCES utilisateur(id_user);


--
-- TOC entry 3926 (class 2606 OID 101748121)
-- Name: releve releve_id_user_fkey; Type: FK CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY releve
    ADD CONSTRAINT releve_id_user_fkey FOREIGN KEY (id_user) REFERENCES utilisateur(id_user);


--
-- TOC entry 3928 (class 2606 OID 101748126)
-- Name: releve_valeur releve_valeur_id_point_suivi_fkey; Type: FK CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY releve_valeur
    ADD CONSTRAINT releve_valeur_id_point_suivi_fkey FOREIGN KEY (id_point_suivi) REFERENCES point_suivi(id_point_suivi);


--
-- TOC entry 3929 (class 2606 OID 101748131)
-- Name: seuil seuil_id_point_suivi_fkey; Type: FK CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY seuil
    ADD CONSTRAINT seuil_id_point_suivi_fkey FOREIGN KEY (id_point_suivi) REFERENCES point_suivi(id_point_suivi);


--
-- TOC entry 3930 (class 2606 OID 101748136)
-- Name: tache_executee tache_executee_id_tache_fkey; Type: FK CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY tache_executee
    ADD CONSTRAINT tache_executee_id_tache_fkey FOREIGN KEY (id_tache) REFERENCES tache_planifiee(id_tache);


--
-- TOC entry 3918 (class 2606 OID 101748141)
-- Name: utilisateur_connexion utilisateur_connexion_id_user_fkey; Type: FK CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY utilisateur_connexion
    ADD CONSTRAINT utilisateur_connexion_id_user_fkey FOREIGN KEY (id_user) REFERENCES utilisateur(id_user);


--
-- TOC entry 3927 (class 2606 OID 101748146)
-- Name: releve_valeur valeur_releve_id_releve_fkey; Type: FK CONSTRAINT; Schema: niveaux; Owner: -
--

ALTER TABLE ONLY releve_valeur
    ADD CONSTRAINT valeur_releve_id_releve_fkey FOREIGN KEY (id_releve) REFERENCES releve(id_releve);


-- Completed on 2020-04-16 14:11:25

--
-- PostgreSQL database dump complete
--

