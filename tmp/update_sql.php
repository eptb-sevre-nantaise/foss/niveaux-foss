<?php
/*
 * Copiez le code SQL ci-dessous et exécutez le dans votre base de données 
 * ATTENTION ce code met à jour le schema 'niveaux', assurez-vous qu'il s'agit bien du schema contenant les tables de l'outil niv'eaux
 */

die();
?>


DROP VIEW niveaux.notification_envoi_infos;
DROP VIEW niveaux.notification_infos;


ALTER TABLE niveaux.notification ADD fonction varchar NULL;
ALTER TABLE niveaux.notification ADD fonction_parametres json NULL;


CREATE OR REPLACE VIEW niveaux.notification_infos
AS SELECT n.id_notification,
n.type_notification,
n.seuil,
n.montant_descendant,
n.date_creation,
n.active,
n.fonction,
n.fonction_parametres,
u.id_user,
u.username,
u.prenom,
u.nom,
u.mail,
u.telephone,
p.id_point_suivi,
p.point_suivi,
p.type_suivi,
p.type_point,
p.type_releve,
p.structure_gestionnaire,
p.carthage_id_cours_eau,
p.carthage_cours_eau,
p.unite,
p.element_mesure
FROM niveaux.notification n
LEFT JOIN niveaux.utilisateur u ON u.id_user = n.id_user
LEFT JOIN niveaux.point_suivi_infos p ON p.id_point_suivi = n.id_point_suivi;



CREATE OR REPLACE VIEW niveaux.notification_envoi_infos
AS SELECT e.id_notification,
e.id_envoi_notification,
e.date_envoi_notification,
e.id_releve,
e.statut,
e.contenu_notification,
e.releve_valeur,
e.releve_valeur_date,
i.type_notification,
i.seuil,
i.montant_descendant,
i.type_suivi,
i.type_point,
i.type_releve,
i.username,
i.point_suivi
FROM niveaux.notification_envoi e
LEFT JOIN niveaux.notification_infos i ON i.id_notification = e.id_notification;