function loadTinyMCEEditor() {

    tinyMCE.init({       
        selector: "textarea",
        menubar: false,
        toolbar: "styleselect | bold,italic,underline,|,bullist,numlist,outdent,indent,| table, |,undo,redo,|,link,code,fullscreen",
        plugins: "code,table,link",        
        language_url : '/js/langs/fr_FR.js',
        language: 'fr_FR'
    });
}



function initMap(num, lng, lat, z) {

    var idmap = 'map' + num;
    map = new L.Map(idmap);

    map.scrollWheelZoom.disable();

    var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    var osmAttrib = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
    var osm = new L.TileLayer(osmUrl, {attribution: osmAttrib});
  
    map.setView(new L.LatLng(lat, lng), z);
    map.addLayer(osm);
    
    var marker = L.marker([lat,lng]).addTo(map);



}




/* version sans le loader, plus light */

function sendData(param, page, emplacement, event, script) {

    script = typeof script !== 'undefined' ? script : false;
    event = typeof event !== 'undefined' ? event : null;

    if (event !== null && event !== 'undefined') {
         event.preventDefault();
    }// using this page stop being refreshing 


    $.ajax({
        type: "POST",
        url: page,
        data: param,
        //async: false,
        success: function (data) {

            $("#" + emplacement).html(data);

            if (script) {
                $("#" + emplacement).find("script").each(function (i) {
                    eval($(this).text());
                });

            }

        }



    });
}